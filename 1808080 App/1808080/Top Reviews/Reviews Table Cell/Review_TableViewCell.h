//
//  Review_TableViewCell.h
//  1808080
//
//  Created by RailsBox on 17/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Review_TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_thumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompanyName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompanyDesc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ratingNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;

@property (strong, nonatomic) IBOutlet UIImageView *imgRate1;
@property (strong, nonatomic) IBOutlet UIImageView *imgRate2;
@property (strong, nonatomic) IBOutlet UIImageView *imgRate3;
@property (strong, nonatomic) IBOutlet UIImageView *imgRate4;
@property (strong, nonatomic) IBOutlet UIImageView *imgRate5;


@end
