//
//  Deals_ViewController.m
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Deals_ViewController.h"
#import "navigator_view.h"
#import "DirectoryServices_View.h"
#import "Deals_CollectionViewCell.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"


#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])



@interface Deals_ViewController ()
{
    navigator_view *navigate;
}


@end

@implementation Deals_ViewController
@synthesize collection_deals;

static NSString * CellIdentifier = @"CellIdentifier";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"Deals_ViewController_6Plus";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if(deviceResult.height == 736)
    {
        UINib *cellNib;
        cellNib = [UINib nibWithNibName:@"Deals_CollectionViewCell_6plus" bundle:nil];
        [collection_deals registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    }
    else
    {
        UINib *cellNib;
        cellNib = [UINib nibWithNibName:@"Deals_CollectionViewCell" bundle:nil];
        [collection_deals registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    }
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


-(void)viewDidAppear:(BOOL)animated
{
    navigate=[[navigator_view alloc]init];
    [self Get_Deals];
}


#pragma mark JSON parsing: get Directory List
-(void)Get_Deals
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"services_bestdeals"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_deals=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"Array Deals:%@",arr_deals);
             
             [collection_deals reloadData];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
}


#pragma mark Collection View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if(deviceResult.height == 736)
    {
        return CGSizeMake(414, 672);
    }
    else if (deviceResult.height==480)
    {
        return CGSizeMake(320, 416);
    }
    else
        return CGSizeMake(320, 504);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arr_deals.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Deals_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.layer.borderWidth=0.5;
    cell.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    
    
    cell.lbl_title.text=[[arr_deals objectAtIndex:indexPath.row]valueForKey:@"pic"];
    cell.lbl_title.text=[[arr_deals objectAtIndex:indexPath.row]valueForKey:@"title"];
    cell.lbl_contact.text=[[arr_deals objectAtIndex:indexPath.row]valueForKey:@"phone"];

    CGRect rect = cell.img_deal.frame;
    
    [cell.img_deal setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_deals objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak Deals_CollectionViewCell *weakCell = cell;
    [cell.img_deal setImageWithURLRequest:request placeholderImage:placeholderImage
                                     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                         
                                         weakCell.img_deal.image = img;
                                         [weakCell setNeedsLayout];
                                         
                                     } failure:nil];
    
    weakCell.img_deal.clipsToBounds=YES;
    
    ///make call
    cell.btn_call.tag=indexPath.row;
    [cell.btn_call addTarget:self action:@selector(Makecall:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}



#pragma mark Make a call
-(IBAction)Makecall:(id)sender
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[[arr_deals objectAtIndex:[sender tag]]valueForKey:@"phone"]]];
    
    [[UIApplication  sharedApplication] openURL:url];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark right menu selection
-(IBAction)Rightmenuselection:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate DirectoryRightNavigation:[sender tag]] animated:YES] ;
}


-(IBAction)back:(id)sender
{
    DirectoryServices_View *mainview=[[DirectoryServices_View alloc]init];
    [self. navigationController pushViewController:mainview animated:YES];
}

@end
