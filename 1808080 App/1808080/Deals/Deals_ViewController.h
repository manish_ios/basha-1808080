//
//  Deals_ViewController.h
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Deals_ViewController : UIViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    
    
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
    
    NSMutableArray *arr_deals;
}


@property (strong, nonatomic) IBOutlet UICollectionView *collection_deals;

@end
