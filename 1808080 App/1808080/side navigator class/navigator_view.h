//
//  navigator_view.h
//  1808080
//
//  Created by RailsBox on 27/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface navigator_view : UIViewController


-(id)navigateToClass:(NSInteger)sender;

-(id)AdvertisementRightNavigation:(NSInteger)sender;

-(id)DirectoryRightNavigation:(NSInteger)sender;

-(id)BookingRightNavigation:(NSInteger)sender;

-(id)ShoppingRightNavigation:(NSInteger)sender;

@end
