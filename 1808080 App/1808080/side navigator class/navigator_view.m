//
//  navigator_view.m
//  1808080
//
//  Created by RailsBox on 27/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "navigator_view.h"
#import "AppDelegate.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

////Main navigation
#import "DirectoryServices_View.h"
#import "Order_ViewController.h"
#import "Shopping_ViewController.h"
#import "Store_ViewController.h"
#import "Booking_ViewController.h"
#import "Advertisement_ViewController.h"

////directory services right view class

#import "TopReview_ViewController.h"
#import "Deals_ViewController.h"
#import "AdvertiseWithUs_ViewController.h"
#import "JoinUs_ViewController.h"
#import "TopCompany_ViewController.h"


///////Advertisement Right View Classes
#import "Ads_Listing_ViewController.h"
#import "AddAds_ViewController.h"
#import "Ads_Deals_ViewController.h"
#import "Advertisement_ViewController.h"


////Booking Right View Classes
#import "Book_SpecialCo_ViewController.h"
#import "BookDeals_ViewController.h"
#import "BookHelp_ViewController.h"
#import "BookJoinUs_ViewController.h"
#import "BookArchive_ViewController.h"

///Shopping Right View Classes
#import "Shop_Archive_ViewController.h"
#import "Shop_deals_ViewController.h"
#import "Shop_Help_ViewController.h"
#import "Shop_JoinUs_ViewController.h"
#import "Shop_special_ViewController.h"


@interface navigator_view ()

@end

@implementation navigator_view

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(id)navigateToClass:(NSInteger)sender
{
    ////left side view
     if (sender==10)
    {
        DirectoryServices_View *directory=[[DirectoryServices_View alloc]init];
        return directory;
    }
    
    else if (sender==20)
    {
        Order_ViewController *order=[[Order_ViewController alloc]init];
        return order;
    }
    
    else if (sender==30)
    {
        Advertisement_ViewController *advert=[[Advertisement_ViewController alloc]init];
        return advert;
    }
    
    else if (sender==40)
    {
        Store_ViewController *store=[[Store_ViewController alloc]init];
        return store;
    }
    else if (sender==50)
    {
        Booking_ViewController *booking=[[Booking_ViewController alloc]init];
        return booking;
    }
    else if (sender==60)
    {
        Shopping_ViewController *shopping=[[Shopping_ViewController alloc]init];
        return shopping;
    }
    else
        return nil;
}


////navigation controls for right side navigation
-(id)AdvertisementRightNavigation:(NSInteger)sender
{
    NSLog(@"Advertisement Left Navigation");
    
    ////left side view
    if (sender==1)
    {
        Main_ViewController *main=[[Main_ViewController alloc]init];
        return main;
    }
    
    else if (sender==2)
    {
        Advertisement_ViewController *Adsmain=[[Advertisement_ViewController alloc]init];
        return Adsmain;
    }
    
    else if (sender==3)
    {
        myAppDelegate.str_ads_rightMenu=@"latest_Ads";
        Ads_Listing_ViewController *adslatest=[[Ads_Listing_ViewController alloc]init];
        return adslatest;
    }
    
    else if (sender==4)
    {
        myAppDelegate.str_ads_rightMenu=@"Most_Ads";
        Ads_Listing_ViewController *adsmost=[[Ads_Listing_ViewController alloc]init];
        return adsmost;
    }
    
    else if (sender==5)
    {
        AddAds_ViewController *addads=[[AddAds_ViewController alloc]init];
        myAppDelegate.str_Segmentstatus=@"AddAds";
        return addads;
    }
    else if (sender==6)
    {
        Ads_Listing_ViewController *adsToday=[[Ads_Listing_ViewController alloc]init];
        myAppDelegate.str_ads_rightMenu=@"AdsToday";
        return adsToday;
    }
    else if (sender==7)
    {
        Ads_Listing_ViewController *adsWeekly=[[Ads_Listing_ViewController alloc]init];
        myAppDelegate.str_ads_rightMenu=@"AdsWeekly";
        return adsWeekly;

    }
    else if (sender==8)
    {
        AddAds_ViewController *PDF=[[AddAds_ViewController alloc]init];
        myAppDelegate.str_Segmentstatus=@"PDF";
        return PDF;
    }

    else if (sender==9)
    {
        Ads_Listing_ViewController *adsFavorite=[[Ads_Listing_ViewController alloc]init];
        myAppDelegate.str_ads_rightMenu=@"AdsFavorite";
        return adsFavorite;
    }
    else if (sender==10)
    {
        Ads_Deals_ViewController *deals=[[Ads_Deals_ViewController alloc]init];
        return deals;
    }
    else
        return nil;
}

#pragma mark Directory Services Right navigation
-(id)DirectoryRightNavigation:(NSInteger)sender
{
    NSLog(@"right view selection");
    
    if (sender==10)
    {
        TopReview_ViewController *reviews=[[TopReview_ViewController alloc]init];
        return reviews;
    }
    else if (sender == 20)
    {
        TopCompany_ViewController *company=[[TopCompany_ViewController alloc]init];
        return company;
    }
    else if (sender == 30)
    {
        JoinUs_ViewController *joinus=[[JoinUs_ViewController alloc]init];
        return joinus;
    }
    else if (sender == 40)
    {
        Deals_ViewController *deals=[[Deals_ViewController alloc]init];
        return deals;
    }
    else if (sender == 50)
    {
        AdvertiseWithUs_ViewController *adsUs=[[AdvertiseWithUs_ViewController alloc]init];
        return adsUs;
    }
    else
        return nil;
    
    return nil;
}

#pragma mark Booking Services Right navigation
-(id)BookingRightNavigation:(NSInteger)sender
{
    ////left side view
    if (sender==10)
    {
        Booking_ViewController *booking=[[Booking_ViewController alloc]init];
        return booking;
    }
    
    else if (sender==20)
    {
        Book_SpecialCo_ViewController *BookSpecial=[[Book_SpecialCo_ViewController alloc]init];
        return BookSpecial;
    }
    
    else if (sender==30)
    {
        BookDeals_ViewController *BookDeal=[[BookDeals_ViewController alloc]init];
        return BookDeal;
    }
    
    else if (sender==40)
    {
        BookArchive_ViewController *BookArchive=[[BookArchive_ViewController alloc]init];
        return BookArchive;
    }
    else if (sender==50)
    {
        BookJoinUs_ViewController *BookJoinUs=[[BookJoinUs_ViewController alloc]init];
        return BookJoinUs;
    }
    else if (sender==60)
    {
        BookHelp_ViewController *BookHelp=[[BookHelp_ViewController alloc]init];
        return BookHelp;
    }
    else
        return nil;
}


#pragma mark Shopping Services Right navigation
-(id)ShoppingRightNavigation:(NSInteger)sender
{
    ////right side view
    if (sender==10)
    {
        Shopping_ViewController *shoping=[[Shopping_ViewController alloc]init];
        return shoping;
    }
    
    else if (sender==20)
    {
        Shop_special_ViewController *ShopSpecial=[[Shop_special_ViewController alloc]init];
        return ShopSpecial;
    }
    
    else if (sender==30)
    {
        Shop_deals_ViewController *ShopDeal=[[Shop_deals_ViewController alloc]init];
        return ShopDeal;
    }
    
    else if (sender==40)
    {
       Shop_Archive_ViewController *ShopArchive=[[Shop_Archive_ViewController alloc]init];
        return ShopArchive;
    }
    else if (sender==50)
    {
        Shop_JoinUs_ViewController *ShopJoinUs=[[Shop_JoinUs_ViewController alloc]init];
        return ShopJoinUs;
    }
    else if (sender==60)
    {
        Shop_Help_ViewController *ShopHelp=[[Shop_Help_ViewController alloc]init];
        return ShopHelp;
    }
    else
        return nil;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
