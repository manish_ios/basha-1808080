//
//  ShopCategory_TableCell.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopCategory_TableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_CatList;
@property (strong, nonatomic) IBOutlet UILabel *lbl_catName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_totalItem;

@end
