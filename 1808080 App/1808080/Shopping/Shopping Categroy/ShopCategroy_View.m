//
//  ShopCategroy_View.m
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "ShopCategroy_View.h"
#import "ShopCategory_TableCell.h"

#import "Shopping_ViewController.h"
#import "Shop_SubCategoryViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "navigator_view.h"
#import "AppDelegate.h"


@interface ShopCategroy_View ()
{
    navigator_view *navigate;
}

@end

@implementation ShopCategroy_View
@synthesize array_category;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"ShopCategroy_View_6Plus";   // iPhone 6+"
        }
        else
        {
            nibNameOrNil = @"ShopCategroy_View";   // iPhone 4,5,6"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
     navigate=[[navigator_view alloc]init];
    
    if (array_category.count>0)
    {
        [_tble_ShopCategory reloadData];
        _tble_ShopCategory.hidden=NO;
    }
    else
    {
        _tble_ShopCategory.hidden=YES;
    }
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}



#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array_category.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"ShopCategory_TableCell";
    
    _tble_ShopCategory.separatorColor=[UIColor clearColor];
    
    ShopCategory_TableCell *cell = (ShopCategory_TableCell *)[_tble_ShopCategory dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        if (deviceResult.height==736)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopCategory_TableCell_6Plus" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopCategory_TableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    
    cell.lbl_catName.text=[[array_category objectAtIndex:indexPath.row] valueForKey:@"title"];
    cell.lbl_totalItem.text=[[array_category objectAtIndex:indexPath.row] valueForKey:@"items"];
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_CatList setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[array_category objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak ShopCategory_TableCell *weakCell = cell;
    [cell.img_CatList setImageWithURLRequest:request placeholderImage:placeholderImage
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                          
                                          weakCell.img_CatList.image = img;
                                          [weakCell setNeedsLayout];
                                          
                                      } failure:nil];
    
    weakCell.img_CatList.clipsToBounds=YES;
    
return cell;
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Shop_SubCategoryViewController *shopSubCat=[[Shop_SubCategoryViewController alloc]init];
    shopSubCat.str_subCategory=[[array_category objectAtIndex:indexPath.row] valueForKey:@"categories_id"];
    [self.navigationController pushViewController:shopSubCat animated:YES];
}


-(void)viewDidLayoutSubviews
{
    if ([_tble_ShopCategory respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tble_ShopCategory setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_tble_ShopCategory respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tble_ShopCategory setLayoutMargins:UIEdgeInsetsZero];
    }
}



#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate ShoppingRightNavigation:[sender tag]] animated:YES] ;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Back Button
-(IBAction)BacktoPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
