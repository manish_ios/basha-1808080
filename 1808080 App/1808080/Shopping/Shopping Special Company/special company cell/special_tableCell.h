//
//  special_tableCell.h
//  1808080
//
//  Created by RailsBox on 10/07/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface special_tableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btn_Call;
@property (strong, nonatomic) IBOutlet UIButton *btn_mail;
@property (strong, nonatomic) IBOutlet UIButton *btn_web;

@property (strong, nonatomic) IBOutlet UIImageView *img_thumb;

@property (strong, nonatomic) IBOutlet UILabel *lbl_CompanyName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompanyDesc;

@end
