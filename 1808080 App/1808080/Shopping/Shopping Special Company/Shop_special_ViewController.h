//
//  Shop_special_ViewController.h
//  1808080
//
//  Created by RailsBox on 07/07/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface Shop_special_ViewController : UIViewController<UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
    
    NSMutableArray *arr_SpecialCo;
    
}

@property (strong, nonatomic) IBOutlet UITableView *tble_special;



@end
