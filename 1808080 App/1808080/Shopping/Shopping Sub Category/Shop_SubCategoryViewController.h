//
//  Shop_SubCategoryViewController.h
//  1808080
//
//  Created by RailsBox on 26/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Shop_SubCategoryViewController : UIViewController<UIGestureRecognizerDelegate>
{
    NSMutableArray *arr_subCategory;
    
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
}

@property (strong, nonatomic) NSString *str_subCategory;

@property (strong, nonatomic) IBOutlet UITableView *tble_subCategory;


@end
