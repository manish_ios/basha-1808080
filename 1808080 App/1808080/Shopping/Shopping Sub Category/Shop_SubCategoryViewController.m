//
//  Shop_SubCategoryViewController.m
//  1808080
//
//  Created by RailsBox on 26/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Shop_SubCategoryViewController.h"
#import "ShopSubCat_TableViewCell.h"
#import "ShopListing_View.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "navigator_view.h"
#import "AppDelegate.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

//id:
//pass:


@interface Shop_SubCategoryViewController ()
{
    navigator_view *navigate;
}

@end

@implementation Shop_SubCategoryViewController

@synthesize tble_subCategory,str_subCategory;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"Shop_SubCategoryViewController_6Plus";   // iPhone 6+"
        }
        else
        {
            nibNameOrNil = @"Shop_SubCategoryViewController";   // iPhone 4,5,6"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];

    [super viewDidLoad];
    
    [self Get_subShopCategory];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}



#pragma mark JSON parsing
-(void)Get_subShopCategory
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_shopping_subcategories"]];
    
    
    NSDictionary *parameters = @{@"categories_id":str_subCategory};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_subCategory=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arrayAdsSubCategroy:%@",arr_subCategory);
             
             tble_subCategory.hidden=NO;
             [tble_subCategory reloadData];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Subcategory Exists" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alert show];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}

#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_subCategory.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"ShopSubCat_TableViewCell";
    
    
    //    [tableView setSeparatorInset:UIEdgeInsetsZero];
    tble_subCategory.separatorColor=[UIColor clearColor];
    
    ShopSubCat_TableViewCell *cell = (ShopSubCat_TableViewCell *)[tble_subCategory dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil)
    {
        
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        if(deviceResult.height == 736)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopSubCat_TableViewCell_6Plus" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShopSubCat_TableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    if (indexPath.row%2==0)
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"ffffff"];
    }
    else
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"fafafa"];
    }
    
    
    cell.lbl_SubName.text=[[arr_subCategory objectAtIndex:indexPath.row]valueForKey:@"title"];
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_subThumb setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_subCategory objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak ShopSubCat_TableViewCell *weakCell = cell;
    [cell.img_subThumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                          
                                          weakCell.img_subThumb.image = img;
                                          [weakCell setNeedsLayout];
                                          
                                      } failure:nil];
    
    weakCell.img_subThumb.clipsToBounds=YES;
    
    
    return cell;
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopListing_View *shopSubCat=[[ShopListing_View alloc]init];
    shopSubCat.str_subcatId=[[arr_subCategory objectAtIndex:indexPath.row] valueForKey:@"subcategories_id"];
    [self.navigationController pushViewController:shopSubCat animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate ShoppingRightNavigation:[sender tag]] animated:YES] ;
    
}


-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
