//
//  ShopSubCat_TableViewCell.h
//  1808080
//
//  Created by RailsBox on 26/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopSubCat_TableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img_subThumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SubName;

@end
