//
//  ShopListing_View.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopListing_View : UIViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
    
    NSMutableArray *arr_Shopdetails;
}

@property (strong, nonatomic) IBOutlet UIButton *Btn_sortBy;

@property (strong, nonatomic) IBOutlet UIView *view_sortBy;

@property (strong, nonatomic) IBOutlet UIView *filter_view;

@property (strong, nonatomic) IBOutlet UIView *filter_Back_View;

@property (strong, nonatomic) IBOutlet UICollectionView *collect_ShopItems;

@property (strong, nonatomic) NSString *str_subcatId;

@property (strong, nonatomic) NSMutableArray *arr_ShopItems;

@property (strong, nonatomic) IBOutlet UILabel *lbl_minPrice,*lbl_MaxPrice;

@property (strong, nonatomic) IBOutlet UIButton *btn_filter;



@end
