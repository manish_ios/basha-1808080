//
//  ShopListing_View.m
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "ShopListing_View.h"
#import "ShopListing_CellView.h"

#import "ShoppingDetails_View.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "navigator_view.h"
#import "CERangeSlider.h"


#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface ShopListing_View ()
{
    navigator_view *navigate;
}

@end

@implementation ShopListing_View


@synthesize collect_ShopItems;
@synthesize str_subcatId;
@synthesize arr_ShopItems;
@synthesize filter_view,lbl_MaxPrice,lbl_minPrice,view_sortBy;



static NSString * CellIdentifier = @"CellIdentifier";


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"ShopListing_View_6Plus";   // iPhone 6+"
        }
        else if(deviceResult.height==480)
        {
            nibNameOrNil = @"ShopListing_View_4";   // iPhone 4,5,6"
        }
        else
        {
            nibNameOrNil = @"ShopListing_View";   // iPhone 4,5,6"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];

    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if(deviceResult.height == 736)
    {
        
        UINib *cellNib;
        cellNib = [UINib nibWithNibName:@"ShopListing_CellView_6Plus" bundle:nil];
        [collect_ShopItems registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    }
    else
    {
        UINib *cellNib;
        cellNib = [UINib nibWithNibName:@"ShopListing_CellView" bundle:nil];
        [collect_ShopItems registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    }
    
    if (arr_ShopItems.count>1)
    {
        [collect_ShopItems reloadData];
    }
    else
    {
        [self Get_subShopSubItems];
    }
    
    [self slider];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}



#pragma mark create slider
-(void)slider
{
    NSUInteger margin = 20;
    CGRect sliderFrame = CGRectMake(margin, 205, filter_view.frame.size.width - margin * 2, 25);
    CERangeSlider* slider1 = [[CERangeSlider alloc] initWithFrame:sliderFrame];
    
    [filter_view addSubview:slider1];
    
    NSLog(@"Max:%2ld",(long)slider1.maximumValue);
    lbl_MaxPrice.text=[NSString stringWithFormat:@"$ %ld",(long)slider1.upperValue];
    lbl_minPrice.text=[NSString stringWithFormat:@"$ %ld",(long)slider1.lowerValue];
    
    [slider1 addTarget:self
                action:@selector(slideValueChanged:)
      forControlEvents:UIControlEventValueChanged];
}

#pragma mark Change Slider Value
- (void)slideValueChanged:(id)control
{
    CERangeSlider* slider = (CERangeSlider*)control;
    NSLog(@"Slider value changed: (%.2ld,%.2ld)",
          (long)slider.lowerValue, (long)slider.upperValue);
    lbl_minPrice.text=[NSString stringWithFormat:@"$ %ld", (long)slider.lowerValue];
    lbl_MaxPrice.text=[NSString stringWithFormat:@"$ %ld", (long)slider.upperValue];
}

#pragma mark JSON parsing
-(void)Get_subShopSubItems
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_shopping_items"]];
    
    
    NSDictionary *parameters = @{@"subcategories_id":str_subcatId};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_ShopItems=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arrayAdsSubCategroy:%@",arr_ShopItems);
             
             collect_ShopItems.hidden=NO;
             [collect_ShopItems reloadData];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}



#pragma mark Collection View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 8;
}


// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);  // top, left, bottom, right
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if(deviceResult.height == 736)
    {
        return CGSizeMake(198, 240);
    }
    else
    {
        return CGSizeMake(150, 190);
    }
}

#pragma mark set total elements
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return arr_ShopItems.count;
}

#pragma mark insert elements
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShopListing_CellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.layer.borderWidth=1;
    cell.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    cell.lbl_itemName.text=[[arr_ShopItems objectAtIndex:indexPath.row] valueForKey:@"title"];
    cell.lbl_Itemprice.text=[NSString stringWithFormat:@"$ %@",[[arr_ShopItems objectAtIndex:indexPath.row] valueForKey:@"price"]];

    CGRect rect = cell.img_item.frame;
    
    [cell.img_item setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_ShopItems objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak ShopListing_CellView *weakCell = cell;
    [cell.img_item setImageWithURLRequest:request placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                       
                                       weakCell.img_item.image = img;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    weakCell.img_item.clipsToBounds=YES;
    
    return cell;
}


#pragma mark collection view didselect
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self Get_getShopdetails:[[arr_ShopItems objectAtIndex:indexPath.row] valueForKey:@"items_id"]];
}


#pragma mark JSON parsing : get Shop Item Details
-(void)Get_getShopdetails:(NSString *)itemId
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_shopping_details"]];
    
    NSDictionary *parameters = @{@"items_id":itemId};
    NSLog(@"parameters %@",parameters);
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         NSLog(@"responseArr:%@",responseArr);
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_Shopdetails= [responseArr mutableCopy];
             NSLog(@"ArrayItemDetails:%@",arr_Shopdetails);
             
             ShoppingDetails_View *shopdetail=[[ShoppingDetails_View alloc]init];
             shopdetail.arr_Shop_details=arr_Shopdetails;
             
             [self.navigationController pushViewController:shopdetail animated:YES];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate ShoppingRightNavigation:[sender tag]] animated:YES] ;
}


#pragma mark Back Button
-(IBAction)BacktoPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)filter:(id)sender
{
    _filter_Back_View.hidden=NO;
    filter_view.hidden=NO;
}


#pragma mark filter apply
-(IBAction)apply:(id)sender
{
    filter_view.hidden=YES;
    _filter_Back_View.hidden=YES;
}

#pragma mark Show Sort By View
-(IBAction)Show_sortBy:(id)sender
{
    if ([sender tag]==0)
    {
        view_sortBy.layer.cornerRadius=5;
        view_sortBy.hidden=NO;
        _Btn_sortBy.tag=1;
    }
    else
    {
        view_sortBy.hidden=YES;
        _Btn_sortBy.tag=0;
    }
}

#pragma mark Sort Array in ascending order
-(IBAction)sortAsc:(UIButton *)sender
{
    
    NSLog(@"ascending:%@",sender.titleLabel.text);
    
    view_sortBy.hidden=YES;
    _Btn_sortBy.tag=0;
   // [_Btn_sortBy setTitle:sender.titleLabel.text forState:UIControlStateNormal];
    
    
    NSSortDescriptor *lowestPriceToHighest = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES];
    arr_ShopItems = [[arr_ShopItems sortedArrayUsingDescriptors:[NSArray arrayWithObject:lowestPriceToHighest]]mutableCopy];
    [collect_ShopItems reloadData];
}

#pragma mark Sort Array in descending order
-(IBAction)sortdesc:(UIButton *)sender
{
    view_sortBy.hidden=YES;
    _Btn_sortBy.tag=0;

   // [_Btn_sortBy setTitle:sender.titleLabel.text forState:UIControlStateNormal];
    
    NSSortDescriptor *lowestPriceToHighest = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];
    
    arr_ShopItems = [[arr_ShopItems sortedArrayUsingDescriptors:[NSArray arrayWithObject:lowestPriceToHighest]]mutableCopy];
    [collect_ShopItems reloadData];
}



@end
