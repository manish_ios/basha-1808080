//
//  ShopListing_CellView.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopListing_CellView : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_item;
@property (strong, nonatomic) IBOutlet UILabel *lbl_itemName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Itemprice;

@end
