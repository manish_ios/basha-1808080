//
//  Shopping_CellsView.m
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Shopping_CellsView.h"

@implementation Shopping_CellsView

@synthesize img_thumb,lbl_catName;


- (void)awakeFromNib {
    
    // Initialization code
    img_thumb .layer.cornerRadius =   img_thumb .frame.size.width / 2;
    // img_profile.clipsToBounds = YES;
    
    img_thumb .layer.borderWidth = 1.0f;
    img_thumb .layer.borderColor = [UIColor whiteColor].CGColor;
    img_thumb .layer.masksToBounds = YES;
    img_thumb .clipsToBounds = YES;

}

@end
