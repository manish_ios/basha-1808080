//
//  OfferZone_ViewCell.h
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferZone_ViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btn_viewDetails;

@property (strong, nonatomic) IBOutlet UIImageView *img_deals;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dealName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dealPrice;

@end
