//
//  OfferZone_ViewCell.m
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "OfferZone_ViewCell.h"

@implementation OfferZone_ViewCell


@synthesize btn_viewDetails;

@synthesize img_deals,lbl_dealName,lbl_dealPrice;


- (void)awakeFromNib {
    
    btn_viewDetails.layer.cornerRadius=2;
    // Initialization code
}

@end
