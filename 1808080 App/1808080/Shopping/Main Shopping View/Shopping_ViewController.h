//
//  Shopping_ViewController.h
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Shopping_ViewController : UIViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *LeftSpeedView;
    IBOutlet UIButton *btn_HomeMenu;
    
    NSMutableArray *arr_category;
    NSMutableArray *arr_mostView;
    NSMutableArray *arr_Shopdetails;
    NSMutableArray *arr_ShopDeals;
    
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collect_Cotegory;
@property (strong, nonatomic) IBOutlet UICollectionView *collect_mostViews;
@property (strong, nonatomic) IBOutlet UICollectionView *collect_offerZone;

@end
