//
//  Shopping_ViewController.m
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Shopping_ViewController.h"

/// Collection view cells
#import "Shopping_CellsView.h"
#import "OfferZone_ViewCell.h"
#import "MostViewSHopping_ViewCell.h"

#import "AppDelegate.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "navigator_view.h"
#import "MBProgressHUD.h"

/// Other Shopping navigation
#import "ShoppingDetails_View.h"
#import "ShopCategroy_View.h"
#import "ShopListing_View.h"

#import "Shop_SubCategoryViewController.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface Shopping_ViewController ()
{
    navigator_view *navigate;
}
@end

@implementation Shopping_ViewController

@synthesize collect_Cotegory,collect_mostViews,collect_offerZone;

static NSString * CellIdentifier = @"CellIdentifier";

static NSString * CellIdentifier2 = @"CellIdentifiers";

static NSString * CellIdentifier3 = @"CellIdentifiers3";


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"Shopping_ViewController_6Plus";   // iPhone 6+"
        }
        else
        {
            nibNameOrNil = @"Shopping_ViewController";   // iPhone 4,5,6"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    UINib *cellNib;
    cellNib = [UINib nibWithNibName:@"Shopping_CellsView" bundle:nil];
    [collect_Cotegory registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    
    UINib *cellNib2;
    cellNib2 = [UINib nibWithNibName:@"MostViewSHopping_ViewCell" bundle:nil];
    [collect_mostViews registerNib:cellNib2 forCellWithReuseIdentifier:CellIdentifier2];
    
    UINib *cellNib3;
    cellNib3 = [UINib nibWithNibName:@"OfferZone_ViewCell" bundle:nil];
    [collect_offerZone registerNib:cellNib3 forCellWithReuseIdentifier:CellIdentifier3];
    
    navigate=[[navigator_view alloc]init];
    
    
    [self get_Shop_Category];
    [self get_Shop_MostView];
    [self get_Shop_Deals];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_HomeMenu.hidden=NO;
    btn_HomeMenu.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}



#pragma mark JSON parsing: get Category List
-(void)get_Shop_Category
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"shopping_categories"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_category=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayCategory:%@",arr_category);
             
             //myAppDelegate.arr_category_advert=arr_category;
             
             [collect_Cotegory reloadData];
             collect_Cotegory.hidden=NO;
         }
         else
         {
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
     }];
}

#pragma mark JSON parsing: get Category List
-(void)get_Shop_MostView
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_shopping_mostview"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_mostView=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arr_mostView:%@",arr_mostView);
             
             [collect_mostViews reloadData];
             collect_mostViews.hidden=NO;
         }
         else
         {
             NSLog(@"not Found");
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
}


#pragma mark JSON parsing: get Category List
-(void)get_Shop_Deals
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"shopping_offerdeals"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_ShopDeals=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arr_DealView:%@",arr_ShopDeals);
             
             [collect_offerZone reloadData];
             collect_offerZone.hidden=NO;
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             NSLog(@"not Found");
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
}



#pragma mark Collection View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView==collect_Cotegory)
    {
        return 0;
    }
        return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView==collect_Cotegory)
    {
        return 0;
    }
    return 10;
}


// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView==collect_offerZone)
    {
        return UIEdgeInsetsMake(0,5,5,5);  // top, left, bottom, right
    }
    else
        return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collect_Cotegory)
    {
        return CGSizeMake(70, 90);
    }
    
   else if (collectionView==collect_offerZone)
    {
        return CGSizeMake(150, 70);
    }
    
    else
        return CGSizeMake(107, 150);
}

#pragma mark set total elements
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == collect_Cotegory)         ///collection category
    {
        return arr_category.count;
    }
    
    else if (collectionView == collect_mostViews)   //most viwed
    {
        return arr_mostView.count;
    }
    
    else
        return arr_ShopDeals.count;              //other offers
}

#pragma mark insert elements
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collect_Cotegory)         //categories
    {
        Shopping_CellsView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        
            cell.lbl_catName.text=[[arr_category objectAtIndex:indexPath.row] valueForKey:@"title"];
            
            CGRect rect = cell.img_thumb.frame;
            
            [cell.img_thumb setFrame:rect];
            
            NSURL *url1 = [NSURL URLWithString:[[arr_category objectAtIndex:indexPath.row]valueForKey:@"pic"]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url1];
            UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
            
            
            __weak Shopping_CellsView *weakCell = cell;
            [cell.img_thumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                               
                                               weakCell.img_thumb.image = img;
                                               [weakCell setNeedsLayout];
                                               
                                           } failure:nil];
            
            weakCell.img_thumb.clipsToBounds=YES;
       
        return cell;
    }
    
    else if (collectionView == collect_mostViews)       ///most viewed
    {
        MostViewSHopping_ViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier2 forIndexPath:indexPath];
        
        cell.lbl_ProName.text=[[arr_mostView objectAtIndex:indexPath.row] valueForKey:@"title"];
        
        cell.lbl_price.text=[NSString stringWithFormat:@"$ %@",[[arr_mostView objectAtIndex:indexPath.row] valueForKey:@"price"]];
        
        CGRect rect = cell.img_mostThumb.frame;
        
        [cell.img_mostThumb setFrame:rect];
        
        NSURL *url1 = [NSURL URLWithString:[[arr_mostView objectAtIndex:indexPath.row]valueForKey:@"pic"]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak MostViewSHopping_ViewCell *weakCell = cell;
        [cell.img_mostThumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                           
                                           weakCell.img_mostThumb.image = img;
                                           [weakCell setNeedsLayout];
                                           
                                       } failure:nil];
        
        weakCell.img_mostThumb.clipsToBounds=YES;
        
        return cell;
    }
    
    else        ////offers and deals
    {
        OfferZone_ViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier3 forIndexPath:indexPath];
        
        cell.lbl_dealName.text=[[arr_ShopDeals objectAtIndex:indexPath.row] valueForKey:@"title"];
        
        cell.lbl_dealPrice.text=[NSString stringWithFormat:@"$ %@",[[arr_ShopDeals objectAtIndex:indexPath.row] valueForKey:@"price"]];

        CGRect rect = cell.img_deals.frame;
        
        [cell.img_deals setFrame:rect];
        
        NSURL *url1 = [NSURL URLWithString:[[arr_ShopDeals objectAtIndex:indexPath.row]valueForKey:@"pic"]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak OfferZone_ViewCell *weakCell = cell;
        [cell.img_deals setImageWithURLRequest:request placeholderImage:placeholderImage
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                               
                                               weakCell.img_deals.image = img;
                                               [weakCell setNeedsLayout];
                                               
                                           } failure:nil];
        
        weakCell.img_deals.clipsToBounds=YES;

        return cell;
    }
}


#pragma mark collection view didselect
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collect_mostViews)
    {
        [self Get_getShopdetails:[[arr_mostView objectAtIndex:indexPath.row]valueForKey:@"items_id"]];
    }
    else if(collectionView == collect_Cotegory)
    {
        Shop_SubCategoryViewController *shopSubCat=[[Shop_SubCategoryViewController alloc]init];
        
        shopSubCat.str_subCategory=[[arr_category objectAtIndex:indexPath.row] valueForKey:@"categories_id"];
        
        [self.navigationController pushViewController:shopSubCat animated:YES];
    }
    else
    {
        [self Get_getShopdetails:[[arr_ShopDeals objectAtIndex:indexPath.row]valueForKey:@"items_id"]];
    }
    
    
    NSLog(@"%ld",(long)indexPath.row);
}

#pragma mark JSON parsing : get Shop Item Details
-(void)Get_getShopdetails:(NSString *)itemId
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_shopping_details"]];
    
    NSDictionary *parameters = @{@"items_id":itemId};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         NSLog(@"responseArr:%@",responseArr);
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_Shopdetails= [responseArr mutableCopy];
             NSLog(@"ArrayItemDetails:%@",arr_Shopdetails);
             
             ShoppingDetails_View *shopdetail=[[ShoppingDetails_View alloc]init];
             shopdetail.arr_Shop_details=arr_Shopdetails;
             
             [self.navigationController pushViewController:shopdetail animated:YES];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark right menu selection
-(IBAction)LeftMenuSelection:(id)sender
{
    LeftSpeedView.hidden=YES;
    [self.navigationController pushViewController:[navigate navigateToClass:[sender tag]] animated:YES] ;
}

#pragma mark View All method
-(IBAction)viewAll:(id)sender
{
    if (arr_mostView.count>0)
    {
        ShopListing_View *listing=[[ShopListing_View alloc]init];
        listing.arr_ShopItems=arr_mostView;
        [self.navigationController pushViewController:listing animated:YES];
    }
}


#pragma mark Show All Category
-(IBAction)ShowAllCategory:(id)sender
{
    if (arr_category.count>0)
    {
        ShopCategroy_View *showcategroy=[[ShopCategroy_View alloc]init];
        showcategroy.array_category=arr_category;
        [self.navigationController pushViewController:showcategroy animated:YES];
    }
}

#pragma mark Show All Category
-(IBAction)ShowAllOfferDeals:(id)sender
{
    if (arr_ShopDeals.count>0)
    {
        ShopListing_View *listing=[[ShopListing_View alloc]init];
        listing.arr_ShopItems=arr_ShopDeals;
        [self.navigationController pushViewController:listing animated:YES];
    }
}


#pragma mark show Left slide view
-(IBAction)showLeftMenu:(id)sender
{
    [self.view addSubview:LeftSpeedView];
    
    if ([sender tag]==1)
    {
        LeftSpeedView.hidden=NO;
        
        LeftSpeedView.frame=CGRectMake(LeftSpeedView.frame.origin.x, 63, LeftSpeedView.frame.size.width, LeftSpeedView.frame.size.height);
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=2;
    }
    
    else
    {
        LeftSpeedView.hidden=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=1;
    }
}

#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    LeftSpeedView.hidden=YES;
    
    btn_HomeMenu.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_HomeMenu.hidden=NO;
        btn_HomeMenu.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}


-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_HomeMenu.hidden=YES;
    btn_HomeMenu.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate ShoppingRightNavigation:[sender tag]] animated:YES] ;
    
}

@end
