//
//  MostViewSHopping_ViewCell.h
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MostViewSHopping_ViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img_mostThumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_price;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ProName;


@end
