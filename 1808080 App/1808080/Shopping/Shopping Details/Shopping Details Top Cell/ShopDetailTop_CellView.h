//
//  ShopDetailTop_CellView.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopDetailTop_CellView : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *imgTopImg;
@property (strong, nonatomic) IBOutlet UIImageView *imgthumbs;

@end
