//
//  ShoppingDetails_View.m
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "ShoppingDetails_View.h"
#import "ShopDetailTop_CellView.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "navigator_view.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface ShoppingDetails_View ()
{
    navigator_view *navigate;
}

@end

@implementation ShoppingDetails_View
@synthesize collect_productImg,collect_TopImg;

@synthesize str_ShopdetailsID;
@synthesize arr_images;
@synthesize lbl_price,lbl_proName,txt_desc;
@synthesize arr_Shop_details;


static NSString * CellIdentifier = @"CellIdentifier";

static NSString * CellIdentifier2 = @"CellIdentifiers";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"ShoppingDetails_View_6Plus";   // iPhone 6+"
        }
        else
        {
            nibNameOrNil = @"ShoppingDetails_View";   // iPhone 4,5,6"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];

    UINib *cellNib;
    cellNib = [UINib nibWithNibName:@"ShopDetailTop_CellView" bundle:nil];
    [collect_productImg registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    
    
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 736)
    {
        UINib *cellNib2;
        cellNib2 = [UINib nibWithNibName:@"ShopDetailTop_CellViewTop_6plus" bundle:nil];
        [collect_TopImg registerNib:cellNib2 forCellWithReuseIdentifier:CellIdentifier2];
    }
    else
    {
        UINib *cellNib2;
        cellNib2 = [UINib nibWithNibName:@"ShopDetailTop_CellViewTop" bundle:nil];
        [collect_TopImg registerNib:cellNib2 forCellWithReuseIdentifier:CellIdentifier2];
    }
    
    
    
    
    NSLog(@"arrShopDetails:%@",arr_Shop_details);
    
    [self show_details];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


#pragma mark JSON parsing
-(void)Get_getdetails
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_shopping_details"]];
    
    NSDictionary *parameters = @{@"items_id":str_ShopdetailsID};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         NSLog(@"responseArr:%@",responseArr);
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_Shop_details=[[responseObject valueForKey:@"Response"] valueForKey:@"data"];
             NSLog(@"ArrayShopDetails:%@",arr_Shop_details);
             
             [self show_details];
             
             arr_images=[[[responseObject valueForKey:@"Response"] valueForKey:@"pic"] objectAtIndex:0];
             
             NSLog(@"arrImages:%@",arr_images);
             
             if (arr_images.count>0)
             {
                 [collect_productImg reloadData];
                 [collect_TopImg reloadData];
             }
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
}


-(void)show_details
{
    NSLog(@"arrShopDetailsssss:%@",[[arr_Shop_details objectAtIndex:0] valueForKey:@"data"]);

    arr_images=[[arr_Shop_details objectAtIndex:0] valueForKey:@"pic"];
    
    NSLog(@"arrImages:%@",arr_images);
    
    if (arr_images.count>0)
    {
        [collect_productImg reloadData];
        [collect_TopImg reloadData];
    }
    
    txt_desc.text=[[[arr_Shop_details objectAtIndex:0]valueForKey:@"data"]valueForKey:@"description"];
    lbl_proName.text=[[[arr_Shop_details objectAtIndex:0]valueForKey:@"data"]valueForKey:@"title"];
    lbl_price.text=[[[arr_Shop_details objectAtIndex:0]valueForKey:@"data"]valueForKey:@"price"];
}


#pragma mark Collection View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(collectionView == collect_productImg)
    {
        return 10;
    }
    else
        return 0;
}


// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(collectionView == collect_productImg)
    {
        return UIEdgeInsetsMake(15,10,15,10);  // top, left, bottom, right
    }
    else
        return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if(deviceResult.height == 736)
    {
        if(collectionView == collect_productImg)
        {
            return CGSizeMake(127,127);
        }
        else
            return CGSizeMake(350, 245);
    }
    else
    {
        if(collectionView == collect_productImg)
        {
            return CGSizeMake(127,127);
        }
        else
            return CGSizeMake(260, 220);
    }
}

#pragma mark set total elements
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == collect_productImg)
    {
        return arr_images.count;
    }
    else
        return arr_images.count;
}

#pragma mark insert elements
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collect_productImg)
    {
        ShopDetailTop_CellView *cell = [collect_productImg dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.layer.borderWidth=1;
        cell.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        
        NSURL *url1 = [NSURL URLWithString:[arr_images objectAtIndex:indexPath.row]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak ShopDetailTop_CellView *weakCell = cell;
        [cell.imgthumbs setImageWithURLRequest:request placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                           
                                           weakCell.imgthumbs.image = img;
                                           [weakCell setNeedsLayout];
                                           
                                       } failure:nil];
        
        weakCell.imgthumbs.clipsToBounds=YES;
        
        return cell;
    }
    else
    {
        ShopDetailTop_CellView *cell = [collect_TopImg dequeueReusableCellWithReuseIdentifier:CellIdentifier2 forIndexPath:indexPath];
        
        NSURL *url1 = [NSURL URLWithString:[arr_images objectAtIndex:indexPath.row]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak ShopDetailTop_CellView *weakCell = cell;
        [cell.imgTopImg setImageWithURLRequest:request placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                           
                                           weakCell.imgTopImg.image = img;
                                           [weakCell setNeedsLayout];
                                           
                                       } failure:nil];
        
        weakCell.imgTopImg.clipsToBounds=YES;
        
        
        
        return cell;
    }
}


#pragma mark collection view didselect
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
}




#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate ShoppingRightNavigation:[sender tag]] animated:YES] ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Back Button
-(IBAction)BacktoPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
