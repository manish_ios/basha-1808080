//
//  ShoppingDetails_View.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingDetails_View : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collect_productImg;

@property (strong, nonatomic) IBOutlet UICollectionView *collect_TopImg;

@property (strong, nonatomic) NSString *str_ShopdetailsID;

@property (strong, nonatomic) NSMutableArray *arr_images;

@property (strong, nonatomic) IBOutlet UILabel *lbl_proName;

@property (strong, nonatomic) IBOutlet UILabel *lbl_price;

@property (strong, nonatomic) IBOutlet UITextView *txt_desc;

@property (strong, nonatomic) NSMutableArray *arr_Shop_details;





@end
