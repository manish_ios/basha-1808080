//
//  Order_ViewController.m
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Order_ViewController.h"
#import "navigator_view.h"

@interface Order_ViewController ()
{
    navigator_view *navigate;
}
@end

@implementation Order_ViewController

- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark right menu selection
-(IBAction)rightmenuselection:(id)sender
{
    LeftSpeedView.hidden=YES;
    [self.navigationController pushViewController:[navigate navigateToClass:[sender tag]] animated:YES] ;
}


#pragma mark show Left slide view
-(IBAction)showLeftMenu:(id)sender
{
    [self.view addSubview:LeftSpeedView];
    
    
    if ([sender tag]==1)
    {
        LeftSpeedView.hidden=NO;
        
        LeftSpeedView.frame=CGRectMake(LeftSpeedView.frame.origin.x, 63, LeftSpeedView.frame.size.width, LeftSpeedView.frame.size.height);
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=2;
    }
    
    else
    {
        LeftSpeedView.hidden=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=1;
    }
}


@end
