//
//  Booking_ViewController.m
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Booking_ViewController.h"
#import "BookList_ViewController.h"
#import "navigator_view.h"
@interface Booking_ViewController ()
{
    navigator_view *navigate;
}
@end

@implementation Booking_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"Booking_ViewController_6Plus";   // iPhone 6+"
        }
        else
        {
            nibNameOrNil = @"Booking_ViewController";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];

    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_HomeMenu.hidden=NO;
    btn_HomeMenu.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)openListing:(id)sender
{
    BookList_ViewController *bookList=[[BookList_ViewController alloc]init];
    [self.navigationController pushViewController:bookList animated:YES];
}


#pragma mark right menu selection
-(IBAction)rightmenuselection:(id)sender
{
    LeftSpeedView.hidden=YES;
    [self.navigationController pushViewController:[navigate BookingRightNavigation:[sender tag]] animated:YES] ;
}

#pragma mark right menu selection
-(IBAction)Leftmenuselection:(id)sender
{
    LeftSpeedView.hidden=YES;
    [self.navigationController pushViewController:[navigate navigateToClass:[sender tag]] animated:YES] ;
}


#pragma mark show Left slide view
-(IBAction)showLeftMenu:(id)sender
{
    [self.view addSubview:LeftSpeedView];
    
    
    if ([sender tag]==1)
    {
        LeftSpeedView.hidden=NO;
        
        LeftSpeedView.frame=CGRectMake(LeftSpeedView.frame.origin.x, 63, LeftSpeedView.frame.size.width, LeftSpeedView.frame.size.height);
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=2;
    }
    
    else
    {
        LeftSpeedView.hidden=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=1;
    }
}

#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    LeftSpeedView.hidden=YES;
    
    btn_HomeMenu.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_HomeMenu.hidden=NO;
        btn_HomeMenu.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}


-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_HomeMenu.hidden=YES;
    btn_HomeMenu.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}



@end
