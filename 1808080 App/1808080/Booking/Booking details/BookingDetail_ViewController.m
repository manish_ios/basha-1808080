//
//  BookingDetail_ViewController.m
//  1808080
//
//  Created by RailsBox on 02/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "BookingDetail_ViewController.h"

@interface BookingDetail_ViewController ()

@end

@implementation BookingDetail_ViewController

@synthesize btn_about,btn_Offers,btn_Services;
@synthesize view_About,view_Offers,view_Services;
@synthesize btn_check1,btn_check2,btn_check3;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Remember Me Button
- (IBAction)Select_Check1:(id)sender
{
    if ([sender tag] == 1)
    {
        btn_check1.tag = 2;
        [btn_check1 setBackgroundImage:[UIImage imageNamed:@"detailcheckboxchecked.png"] forState:UIControlStateNormal];
    }
    else
    {
        btn_check1.tag = 1;
        [btn_check1 setBackgroundImage:[UIImage imageNamed:@"detailcheckbox.png"] forState:UIControlStateNormal];
    }
}


#pragma mark Remember Me Button
- (IBAction)Select_Check2:(id)sender
{
    if ([sender tag] == 1)
    {
        btn_check2.tag = 2;
        [btn_check2 setBackgroundImage:[UIImage imageNamed:@"detailcheckboxchecked.png"] forState:UIControlStateNormal];
    }
    else
    {
        btn_check2.tag = 1;
        [btn_check2 setBackgroundImage:[UIImage imageNamed:@"detailcheckbox.png"] forState:UIControlStateNormal];
    }
}



#pragma mark Remember Me Button
- (IBAction)Select_Check3:(id)sender
{
    if ([sender tag] == 1)
    {
        btn_check3.tag = 2;
        [btn_check3 setBackgroundImage:[UIImage imageNamed:@"detailcheckboxchecked.png"] forState:UIControlStateNormal];
    }
    else
    {
        btn_check3.tag = 1;
        [btn_check3 setBackgroundImage:[UIImage imageNamed:@"detailcheckbox.png"] forState:UIControlStateNormal];
    }
}




#pragma mark Select About
-(IBAction)SelectAbout:(id)sender
{
    [btn_about setBackgroundImage:[UIImage imageNamed:@"bkdetailtabactive.png"] forState:UIControlStateNormal];
    
    [btn_Services setBackgroundImage:[UIImage imageNamed:@"bkdetailtab.png"] forState:UIControlStateNormal];

    [btn_Offers setBackgroundImage:[UIImage imageNamed:@"bkdetailtab.png"] forState:UIControlStateNormal];

    
    view_Services.hidden=YES;
    view_Offers.hidden=YES;
    view_About.hidden=NO;
}

#pragma mark Select Services
-(IBAction)SelectServices:(id)sender
{
    [btn_Services setBackgroundImage:[UIImage imageNamed:@"bkdetailtabactive.png"] forState:UIControlStateNormal];
    
    [btn_about setBackgroundImage:[UIImage imageNamed:@"bkdetailtab.png"] forState:UIControlStateNormal];
    
    
    [btn_Offers setBackgroundImage:[UIImage imageNamed:@"bkdetailtab.png"] forState:UIControlStateNormal];
    
    
    view_Services.hidden=NO;
    view_Offers.hidden=YES;
    view_About.hidden=YES;
}

#pragma mark Select Offers
-(IBAction)SelectOffers:(id)sender

{
    [btn_Offers setBackgroundImage:[UIImage imageNamed:@"bkdetailtabactive.png"] forState:UIControlStateNormal];
    
    [btn_Services setBackgroundImage:[UIImage imageNamed:@"bkdetailtab.png"] forState:UIControlStateNormal];
    
    [btn_about setBackgroundImage:[UIImage imageNamed:@"bkdetailtab.png"] forState:UIControlStateNormal];

    
    
    view_Services.hidden=YES;
    view_Offers.hidden=NO;
    view_About.hidden=YES;
}




-(IBAction)BackToPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
