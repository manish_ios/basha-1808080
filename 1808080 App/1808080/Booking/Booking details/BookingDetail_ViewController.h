//
//  BookingDetail_ViewController.h
//  1808080
//
//  Created by RailsBox on 02/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingDetail_ViewController : UIViewController

{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
}



@property (strong, nonatomic) IBOutlet UIButton *btn_about;
@property (strong, nonatomic) IBOutlet UIButton *btn_Services;
@property (strong, nonatomic) IBOutlet UIButton *btn_Offers;

@property (strong, nonatomic) IBOutlet UIView *view_About;
@property (strong, nonatomic) IBOutlet UIView *view_Offers;
@property (strong, nonatomic) IBOutlet UIView *view_Services;


@property (strong, nonatomic) IBOutlet UIButton *btn_check1;
@property (strong, nonatomic) IBOutlet UIButton *btn_check2;
@property (strong, nonatomic) IBOutlet UIButton *btn_check3;

@end
