//
//  BookDeals_ViewController.h
//  1808080
//
//  Created by RailsBox on 23/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookDeals_ViewController : UIViewController<UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
    
    NSMutableArray *arr_Book_deals;
}


@property (strong, nonatomic) IBOutlet UICollectionView *collection_Book_deals;
@end
