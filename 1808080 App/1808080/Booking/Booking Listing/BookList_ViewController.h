//
//  BookList_ViewController.h
//  1808080
//
//  Created by RailsBox on 02/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookList_ViewController : UIViewController

{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
}


@property (strong, nonatomic) IBOutlet UITableView *tble_booking;


@end
