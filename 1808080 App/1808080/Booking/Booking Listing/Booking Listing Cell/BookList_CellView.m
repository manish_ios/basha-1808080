//
//  BookList_CellView.m
//  1808080
//
//  Created by RailsBox on 02/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "BookList_CellView.h"

@implementation BookList_CellView
@synthesize lbl_DocExp,lbl_DocName,lbl_Hospital,img_header,img_profile,btn_book;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    img_profile.layer.cornerRadius = img_profile.frame.size.width / 2;
   // img_profile.clipsToBounds = YES;
    
    img_profile.layer.borderWidth = 2.0f;
    img_profile.layer.borderColor = [UIColor whiteColor].CGColor;
    img_profile.layer.masksToBounds = YES;
    img_profile.clipsToBounds = YES;
    
    
    // Configure the view for the selected state
}

@end
