//
//  BookList_CellView.h
//  1808080
//
//  Created by RailsBox on 02/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookList_CellView : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UIImageView *img_header;
@property (strong, nonatomic) IBOutlet UILabel *lbl_DocName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Hospital;
@property (strong, nonatomic) IBOutlet UILabel *lbl_DocExp;
@property (strong, nonatomic) IBOutlet UIButton *btn_book;


@end
