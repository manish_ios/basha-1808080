//
//  AdvertiseWithUs_ViewController.h
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvertiseWithUs_ViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    
    
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
}

@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (strong, nonatomic) IBOutlet UIImageView *img_advertise;

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView_Sell;

-(IBAction)chooseExistingClicked:(id)sender;
-(IBAction)takePhotoClicked:(id)sender;
-(IBAction)RemoveImage:(id)sender;
-(IBAction)Submit:(id)sender;

@end
