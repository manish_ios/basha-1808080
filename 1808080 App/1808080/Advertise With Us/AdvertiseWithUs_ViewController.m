//
//  AdvertiseWithUs_ViewController.m
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "AdvertiseWithUs_ViewController.h"
#import "navigator_view.h"
#import "DirectoryServices_View.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "AppDelegate.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AdvertiseWithUs_ViewController ()
{
    navigator_view *navigate;
    NSMutableData *receivedData;
    NSData *img_data;
}

@end

@implementation AdvertiseWithUs_ViewController

@synthesize txtName,txtPhoneNo,img_advertise,theScrollView_Sell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"AdvertiseWithUs_ViewController";   // iPhone 4"
        }
        else
        {
            nibNameOrNil = @"AdvertiseWithUs_ViewController_normal";   // iPhone 4"
        }
    }
    
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    navigate=[[navigator_view alloc]init];    
}


- (void)viewDidLoad
{
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoards:)];
    gestureRecognizer.delegate = self;
    
    [theScrollView_Sell addGestureRecognizer:gestureRecognizer];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}

/////////////////////////
-(void) hideKeyBoards:(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [txtPhoneNo resignFirstResponder];
    [txtName resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtPhoneNo resignFirstResponder];
    [txtName resignFirstResponder];
    
    return YES;
}


#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark right menu selection
-(IBAction)Rightmenuselection:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate DirectoryRightNavigation:[sender tag]] animated:YES] ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender
{
    DirectoryServices_View *mainview=[[DirectoryServices_View alloc]init];
    [self. navigationController pushViewController:mainview animated:YES];
}


-(IBAction)RemoveImage:(id)sender
{
    img_data=nil;
    img_advertise.image=[UIImage imageNamed:@"Detailimg.png"];
}


-(IBAction)Submit:(id)sender
{
    if (txtName.text.length==0||[txtName.text isEqualToString:@" "])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Name required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if(txtPhoneNo.text.length==0||[txtPhoneNo.text isEqualToString:@" "])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Phone number required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if(img_data.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Image required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
       [self API_Post_Advertise];
       
    }
}




#pragma mark Image from Camara and Gallery
-(IBAction)chooseExistingClicked:(id)sender
{
    UIImagePickerController *controller;
    if (controller != nil) {
        
        controller = nil;
    }
    
    controller = [[UIImagePickerController alloc]init];
    controller.delegate = self;
    controller.allowsEditing = YES;
    
    controller.navigationBar.barStyle = UIBarStyleDefault;
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([UIImagePickerController isSourceTypeAvailable: sourceType]) {
        
        controller.sourceType = sourceType;
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        
        UIAlertView*  alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Camera not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}


-(IBAction)takePhotoClicked:(id)sender
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
        
        
    }
    
    else
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    img_advertise.image = chosenImage;
    img_data = [NSData dataWithData:UIImageJPEGRepresentation(chosenImage,0.1f)];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark JSON parsing: Advertise with Us

-(void)API_Post_Advertise
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"services_advertise_with_us"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
    
    ///////////////////////////Name
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"name\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[txtName.text dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////////////////////////////////////////////Phone Number
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"phone\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txtPhoneNo.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////////////////////////////////////////////Image
    if (img_data)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\"ipodfile.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:img_data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:img_data];
    }
    
    /////////////////////////submit////
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = nil;
    receivedData = [[NSMutableData alloc] init];
    
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString*  text = [[NSString alloc] initWithData:receivedData encoding: NSUTF8StringEncoding];
    
    NSArray  *arr1 = [NSJSONSerialization JSONObjectWithData:[text dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    NSArray *res=[arr1 valueForKey:@"Response"];
    NSLog(@"Ad response %@",res);
    
    if([[[res objectAtIndex:0] valueForKey:@"success"] isEqual:@"S"])
    {
        [self back:self];
    }
    else
    {
        
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    NSLog(@"Error %@",error);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
}





@end
