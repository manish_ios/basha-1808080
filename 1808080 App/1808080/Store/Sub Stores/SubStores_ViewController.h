//
//  SubStores_ViewController.h
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubStores_ViewController : UIViewController
{
    NSMutableArray *arr_SubCategory;
}

@property (strong, nonatomic) IBOutlet UITableView *tble_StoreSub;
@property (strong, nonatomic) NSString *str_StoreCatagory;
@property (strong, nonatomic) NSString *str_StoreCatagoryName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_header;


@end
