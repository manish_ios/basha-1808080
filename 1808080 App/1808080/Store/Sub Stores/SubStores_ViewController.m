//
//  SubStores_ViewController.m
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "SubStores_ViewController.h"
#import "SubStore_Cell_View.h"
#import "StoreProducts_View.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface SubStores_ViewController ()

@end

@implementation SubStores_ViewController

@synthesize str_StoreCatagory,tble_StoreSub;


- (void)viewDidLoad
{
    tble_StoreSub.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    _lbl_header.text=_str_StoreCatagoryName;
    
    
    [self get_Store_subCategory];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewDidAppear:(BOOL)animated
{
    
    //[self get_Store_subCategory];
}

#pragma mark JSON parsing: get Directory List
-(void)get_Store_subCategory
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_store_subcategories"]];
    
    
    NSDictionary *parameters = @{@"categories_id":str_StoreCatagory};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_SubCategory=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arrayDirectory:%@",arr_SubCategory);
             
             tble_StoreSub.hidden=NO;
             [tble_StoreSub reloadData];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}


#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_SubCategory.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"SubStore_Cell_View";
    
    
//    [tableView setSeparatorInset:UIEdgeInsetsZero];
    tble_StoreSub.separatorColor=[UIColor clearColor];
    
    SubStore_Cell_View *cell = (SubStore_Cell_View *)[tble_StoreSub dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubStore_Cell_View" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row%2==0)
    {
       cell.backgroundColor=[myAppDelegate colorWithHexString:@"ffffff"];
    }
    else
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"fafafa"];
    }
    
    
    cell.lbl_Name.text=[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"title"];
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_thumb setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak SubStore_Cell_View *weakCell = cell;
    [cell.img_thumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                       
                                       weakCell.img_thumb.image = img;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    weakCell.img_thumb.clipsToBounds=YES;
    
    
    return cell;
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoreProducts_View *storeProduct=[[StoreProducts_View alloc]init];
    
    storeProduct.str_StoreProductCatagory=[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"subcategory_id"];
    
    storeProduct.str_StoreProductCatagoryName=[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"title"];
    
    [self.navigationController pushViewController:storeProduct animated:YES];
}


-(void)viewDidLayoutSubviews
{
    if ([tble_StoreSub respondsToSelector:@selector(setSeparatorInset:)]) {
        [tble_StoreSub setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tble_StoreSub respondsToSelector:@selector(setLayoutMargins:)]) {
        [tble_StoreSub setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(IBAction)backToPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
