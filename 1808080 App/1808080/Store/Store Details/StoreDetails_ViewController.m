//
//  StoreDetails_ViewController.m
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "StoreDetails_ViewController.h"
#import "UIImageView+AFNetworking.h"

@interface StoreDetails_ViewController ()

@end

@implementation StoreDetails_ViewController

@synthesize arr_details,mapview,currentLocation,img_view;


#pragma mark ViewDidLoad
- (void)viewDidLoad
{
    NSLog(@"arr:%@",arr_details);
    
    /////
    
    coordinate.latitude = (CGFloat)[[arr_details valueForKey:@"latitude"] floatValue];;
    coordinate.longitude = (CGFloat)[[arr_details valueForKey:@"longitude"] floatValue];;
    
    
    NSLog(@"lalitude cor:%f",coordinate.latitude);
    
    
    MKCoordinateRegion myRegion;
    
    CLLocationCoordinate2D myCoordinate;
    myCoordinate.latitude  = coordinate.latitude;
    myCoordinate.longitude = coordinate.longitude;
    MKCoordinateSpan mySpan;
    mySpan.latitudeDelta=0.30f;
    mySpan.longitudeDelta = 0.30f;
    myRegion.center = myCoordinate;
    myRegion.span = mySpan;
    [mapview setRegion:myRegion animated:YES];
    
    MKPointAnnotation *annotation =[[MKPointAnnotation alloc]init];
    annotation.coordinate =myCoordinate;
    
    annotation.title=[arr_details valueForKey:@"location"];
    [mapview setRegion:myRegion animated:true];
    [mapview addAnnotation:annotation];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark ViewDidAppear
-(void)viewDidAppear:(BOOL)animated
{
    [self showdetails];
}


#pragma mark Show Details
-(void)showdetails
{
    _lbl_BName.text=[arr_details valueForKey:@"title"];
    _lbl_address.text=[arr_details valueForKey:@"location"];
    _txtView_Desc.text=[arr_details valueForKey:@"description"];
    _lbl_Header.text=[arr_details valueForKey:@"title"];
    
    
    NSURL *url = [NSURL URLWithString:[arr_details valueForKey:@"pic"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIImage *placeholderImage = [UIImage imageNamed:@"Detailimg.png"];
    [img_view setImageWithURLRequest:request
                     placeholderImage:placeholderImage
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  
                                  img_view.image = image;
                                  
                              } failure:nil];
}

#pragma mark Make a call
-(IBAction)DetailsButtons:(id)sender
{
    if ([sender tag]==1)    ////make call
    {
        NSLog(@"Make Call");
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[arr_details valueForKey:@"phone"]]];
        
        [[UIApplication  sharedApplication] openURL:url];
    }
    
    else if ([sender tag]==2)       ////show route
    {
        NSLog(@"Route");
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
        
        [locationManager requestAlwaysAuthorization];
    }
    
    else if ([sender tag]==3)       ///Open web link
    {
        NSLog(@"open web");
        NSURL *url_flicker = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@",[arr_details valueForKey:@"website"]]];
        
        if (![[UIApplication sharedApplication] openURL:url_flicker]) {
            NSLog(@"%@%@",@"Failed to open url:",[url_flicker description]);
        }
    }
    
    else if ([sender tag]==4)       ///send mail
    {
        NSLog(@"send mail");
        // Email Content
        NSString *messageBody =@"Check out this App at";
        
        // To address
        //NSArray *toRecipents = [arr_details valueForKey:@"email"];
        NSArray *toRecipents = [NSArray arrayWithObject:[arr_details valueForKey:@"email"]];
        
        NSLog(@"toRecipents %@ ",toRecipents);
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:@"Share App Information"];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
}

#pragma mark email composer delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//////
////////////////
////map view////
////////////////


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Alert!" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    /////
    ///////
    coordinate.latitude=(CGFloat)[[arr_details valueForKey:@"latitude"] floatValue];
    coordinate.longitude=(CGFloat)[[arr_details valueForKey:@"longitude"] floatValue];
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation=newLocation;
    
    if (currentLocation != nil)
    {
        NSLog(@"location is ");
        str_latitiude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        str_longitude = [NSString stringWithFormat:@"%f", coordinate.longitude] ;
        [locationManager stopUpdatingLocation];
        
        
        MKCoordinateRegion region;
        region.center=currentLocation.coordinate;
        MKCoordinateSpan span;
        span.latitudeDelta=0.01;
        span.longitudeDelta=0.01;
        region.span=span;
        mapview.showsUserLocation=YES;
        mapview.mapType=MKMapTypeStandard;
        [mapview setRegion:region animated:TRUE];
        ////////////////////
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        CLLocation *currentLocations = newLocation;
        
        if (currentLocations != nil)
            NSLog(@"longitude = %.8f\nlatitude = %.8f", currentLocations.coordinate.longitude,currentLocations.coordinate.latitude);
        
        // stop updating location in order to save battery power
        [locationManager stopUpdatingLocation];
        
        
        [geocoder reverseGeocodeLocation:currentLocations completionHandler:^(NSArray *placemarks, NSError *error)
         {
             //  NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
             
             [mapview.userLocation setTitle:@"MY LOCATION"];
             [mapview.userLocation setSubtitle:_str_placemarks];
         }];
    }
    else
    {
        [locationManager startUpdatingLocation];
    }
}




- (MKAnnotationView *)mapView:(MKMapView *)_mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[_mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID] ;
    }
    
    annotationView.image = [UIImage imageNamed:@"pin.png"];//add any image which you want to show on map instead of red pins
    
    ////
    //////
    
    [annotationView setCanShowCallout:YES];
    
    [annotationView setSelected:YES animated:YES];
    ///////
    return annotationView;
}
/////


#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)BackToSub:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
