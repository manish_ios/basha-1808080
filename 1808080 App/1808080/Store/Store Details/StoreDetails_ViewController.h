//
//  StoreDetails_ViewController.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MessageUI.h>


@interface StoreDetails_ViewController : UIViewController
<MKMapViewDelegate,CLLocationManagerDelegate,MFMailComposeViewControllerDelegate>
{
    //current location
    CLLocationCoordinate2D coordinate;
    CLLocationManager *locationManager;
    NSString *currentLatitudeStr, *currentLongitudeStr;
    NSString *str_latitiude,*str_longitude;
    
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    
    
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
}



@property (strong, nonatomic) IBOutlet MKMapView *mapview;
@property (strong, nonatomic) CLLocation *currentLocation;

@property (strong, nonatomic) IBOutlet UITextView *txtView_Desc;
@property (strong ,nonatomic) IBOutlet UILabel *lbl_BName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UIImageView *img_view;
@property (strong, nonatomic) NSMutableArray *arr_details;
@property (strong ,nonatomic) IBOutlet UILabel *lbl_Header;

@property (strong , nonatomic) NSString *str_placemarks;



@end
