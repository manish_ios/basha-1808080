//
//  Store_ViewController.m
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Store_ViewController.h"
#import "MainStoreCell_ViewCell.h"
#import "SubStores_ViewController.h"

#import "navigator_view.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface Store_ViewController ()
{
    navigator_view *navigate;
}


@end

@implementation Store_ViewController

- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];

    [self get_Store];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self get_Store];
}

#pragma mark JSON parsing: get Directory List
-(void)get_Store
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"store_categories"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_AllStore=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayStore:%@",arr_AllStore);
             
             [_tble_Store reloadData];
             _tble_Store.hidden=NO;
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             NSLog(@"not Found");
             [MBProgressHUD hideHUDForView:self.view animated:YES];

         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];

     }];
}

#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 116;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_AllStore.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"MainStoreCell_ViewCell";
    
    _tble_Store.separatorColor=[UIColor clearColor];
    
    MainStoreCell_ViewCell *cell = (MainStoreCell_ViewCell *)[_tble_Store dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MainStoreCell_ViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_thumb setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_AllStore objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"Detailimg.png"];
    
    
    __weak MainStoreCell_ViewCell *weakCell = cell;
    [cell.img_thumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                       
                                       weakCell.img_thumb.image = img;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    weakCell.img_thumb.clipsToBounds=YES;
    

    
    NSURL *url2 = [NSURL URLWithString:[[arr_AllStore objectAtIndex:indexPath.row]valueForKey:@"logo"]];
    
    NSURLRequest *request1 = [NSURLRequest requestWithURL:url2];
    UIImage *placeholderImage1 = [UIImage imageNamed:@"listimg.png"];
    
    [cell.img_logo setImageWithURLRequest:request1 placeholderImage:placeholderImage1
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                       
                                       weakCell.img_logo.image = img;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    weakCell.img_logo.clipsToBounds=YES;
    
    
    cell.lbl_StoreName.text=[[arr_AllStore objectAtIndex:indexPath.row]valueForKey:@"title"];
    
    return cell;
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubStores_ViewController *subStore=[[SubStores_ViewController alloc]init];
    
    subStore.str_StoreCatagory=[[arr_AllStore objectAtIndex:indexPath.row]valueForKey:@"category_id"];
    subStore.str_StoreCatagoryName=[[arr_AllStore objectAtIndex:indexPath.row]valueForKey:@"title"];

    [self.navigationController pushViewController:subStore animated:YES];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark right menu selection
-(IBAction)rightmenuselection:(id)sender
{
    LeftSpeedView.hidden=YES;
    [self.navigationController pushViewController:[navigate navigateToClass:[sender tag]] animated:YES] ;
}


#pragma mark show Left slide view
-(IBAction)showLeftMenu:(id)sender
{
    [self.view addSubview:LeftSpeedView];
    
    
    if ([sender tag]==1)
    {
        LeftSpeedView.hidden=NO;
        
        LeftSpeedView.frame=CGRectMake(LeftSpeedView.frame.origin.x, 63, LeftSpeedView.frame.size.width, LeftSpeedView.frame.size.height);
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=2;
    }
    
    else
    {
        LeftSpeedView.hidden=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=1;
    }
}


@end
