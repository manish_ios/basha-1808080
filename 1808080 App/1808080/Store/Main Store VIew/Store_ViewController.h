//
//  Store_ViewController.h
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Store_ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *LeftSpeedView;
    IBOutlet UIButton *btn_HomeMenu;
    NSMutableArray *arr_AllStore;
}

@property (strong, nonatomic) IBOutlet UITableView *tble_Store;


@end
