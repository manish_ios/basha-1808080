//
//  MainStoreCell_ViewCell.m
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "MainStoreCell_ViewCell.h"

@implementation MainStoreCell_ViewCell

@synthesize img_logo,img_thumb,lbl_StoreName,img_logoBack;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    img_logoBack.layer.cornerRadius = img_logoBack.frame.size.width / 2;
    // img_profile.clipsToBounds = YES;
    
    img_logoBack.layer.borderWidth = 2.0f;
    img_logoBack.layer.borderColor = [UIColor whiteColor].CGColor;
    img_logoBack.layer.masksToBounds = YES;
    img_logoBack.clipsToBounds = YES;

    
    // Configure the view for the selected state
}

@end
