//
//  MainStoreCell_ViewCell.h
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainStoreCell_ViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_logo;
@property (strong, nonatomic) IBOutlet UIImageView *img_thumb;
@property (strong, nonatomic) IBOutlet UIView *img_logoBack;

@property (strong, nonatomic) IBOutlet UILabel *lbl_StoreName;

@end
