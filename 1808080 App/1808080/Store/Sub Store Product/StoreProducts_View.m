//
//  StoreProducts_View.m
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "StoreProducts_View.h"
#import "SubProduct_Cell.h"
#import "StoreDetails_ViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])


@interface StoreProducts_View ()

@end

@implementation StoreProducts_View

@synthesize str_StoreProductCatagory,str_StoreProductCatagoryName;


- (void)viewDidLoad {
    
    [self get_Store_subCategoryProduct];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    _lbl_header.text=str_StoreProductCatagoryName;
   // [self get_Store_subCategoryProduct];
}

#pragma mark JSON parsing: get Directory List
-(void)get_Store_subCategoryProduct
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_store_products"]];
    
    
    NSDictionary *parameters = @{@"subcategory_id":str_StoreProductCatagory};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_SubCategory=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arrayDirectory:%@",arr_SubCategory);
             
             _tble_StoreSubProduct.hidden=NO;
             [_tble_StoreSubProduct reloadData];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}


#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_SubCategory.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"SubProduct_Cell";
    
    
    //    [tableView setSeparatorInset:UIEdgeInsetsZero];
    _tble_StoreSubProduct.separatorColor=[UIColor clearColor];
    
    SubProduct_Cell *cell = (SubProduct_Cell *)[_tble_StoreSubProduct dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubProduct_Cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row%2==0)
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"ffffff"];
    }
    else
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"fafafa"];
    }
    
    
    cell.lbl_Name.text=[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"title"];
    cell.lbl_desc.text=[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"description"];
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_thumb setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_SubCategory objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak SubProduct_Cell *weakCell = cell;
    [cell.img_thumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                       
                                       weakCell.img_thumb.image = img;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    weakCell.img_thumb.clipsToBounds=YES;
    
    
    return cell;
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoreDetails_ViewController *details=[[StoreDetails_ViewController alloc]init];
    details.arr_details=[arr_SubCategory objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:details animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backToPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
