//
//  StoreProducts_View.h
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreProducts_View : UIViewController
{
    NSMutableArray *arr_SubCategory;
}

@property (strong, nonatomic) IBOutlet UITableView *tble_StoreSubProduct;
@property (strong, nonatomic) NSString *str_StoreProductCatagory;
@property (strong, nonatomic) NSString *str_StoreProductCatagoryName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_header;


@end
