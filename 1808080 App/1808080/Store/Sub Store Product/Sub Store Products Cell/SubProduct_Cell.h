//
//  SubProduct_Cell.h
//  1808080
//
//  Created by RailsBox on 04/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubProduct_Cell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbl_Name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_desc;
@property (strong, nonatomic) IBOutlet UIImageView *img_thumb;


@end
