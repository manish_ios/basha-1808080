//
//  JoinUs_ViewController.m
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "JoinUs_ViewController.h"
#import "navigator_view.h"
#import "DirectoryServices_View.h"
#import "AppDelegate.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface JoinUs_ViewController ()
{
    navigator_view *navigate;
}
@end

@implementation JoinUs_ViewController

@synthesize txtName,txtCompany,txtDesc,txtPhone,theScrollView_Sell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"JoinUs_ViewController_6Plus";   // iPhone 6 Plus"
        }
    }
    
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoards:)];
    gestureRecognizer.delegate = self;
    
    [theScrollView_Sell addGestureRecognizer:gestureRecognizer];
    
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}



#pragma mark Hide Keyboard
-(void) hideKeyBoards:(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark ViewWillAppear
-(void)viewDidAppear:(BOOL)animated
{
    navigate=[[navigator_view alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark text view delegates
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([txtDesc.text isEqualToString:@"Message"])
    {
        txtDesc.text = @"";
        txtDesc.textColor = [UIColor darkGrayColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if(txtDesc.text.length==0)
    {
        txtDesc.text = @"Message";
        txtDesc.textColor = [UIColor lightGrayColor];
        [txtDesc resignFirstResponder];
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtPhone resignFirstResponder];
    [txtName resignFirstResponder];
    [txtCompany resignFirstResponder];
    [txtDesc resignFirstResponder];
    
    return YES;
}


#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

#pragma mark Show Views
-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}

#pragma mark right menu selection
-(IBAction)Rightmenuselection:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate DirectoryRightNavigation:[sender tag]] animated:YES] ;
}

#pragma mark Back Button Click
-(IBAction)back:(id)sender
{
    DirectoryServices_View *mainview=[[DirectoryServices_View alloc]init];
    [self. navigationController pushViewController:mainview animated:YES];
}




#pragma mark IBAction
-(IBAction)Submit:(id)sender
{
    if (txtName.text.length==0|| txtPhone.text.length==0 || txtCompany.text.length==0 || txtDesc.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"All fields are mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self API_Post_JoinUS];
    }
}


#pragma mark JSON parsing: Join US
-(void)API_Post_JoinUS
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"services_join_with_us"]];
    NSDictionary *parameters = @{@"name":txtName.text,@"phone":txtPhone.text,@"company_name":txtCompany.text,@"message":txtDesc.text};
    NSLog(@"parameters %@",parameters);
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];

    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSArray *resArray=responseObject;
         if ([[[[resArray valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [self back:self];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
}


@end
