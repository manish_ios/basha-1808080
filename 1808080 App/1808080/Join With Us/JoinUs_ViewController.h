//
//  JoinUs_ViewController.h
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JoinUs_ViewController : UIViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    
    
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
}

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView_Sell;

@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtCompany;
@property (strong, nonatomic) IBOutlet UITextView *txtDesc;

-(IBAction)Submit:(id)sender;

@end
