//
//  AdsSubCategory_Cell.h
//  1808080
//
//  Created by RailsBox on 05/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdsSubCategory_Cell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_subThumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SubName;

@end
