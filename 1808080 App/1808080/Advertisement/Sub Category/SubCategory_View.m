//
//  SubCategory_View.m
//  1808080
//
//  Created by RailsBox on 01/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "SubCategory_View.h"
#import "AdsSubCategory_Cell.h"
#import "Ads_Listing_ViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

#import "AppDelegate.h"
#import "MBProgressHUD.h"

#import "navigator_view.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface SubCategory_View ()
{
    navigator_view *navigate;
}

@end

@implementation SubCategory_View

@synthesize tble_subCat;
@synthesize str_AdsCategoryID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"SubCategory_View_6Plus";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [self Get_subAdsCategory];
    
    navigate=[[navigator_view alloc]init];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}

#pragma mark JSON parsing
-(void)Get_subAdsCategory
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_ads_subcategories"]];
    
    
    NSDictionary *parameters = @{@"categories_id":str_AdsCategoryID};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_AdssubCategory=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arrayAdsSubCategroy:%@",arr_AdssubCategory);
             
             tble_subCat.hidden=NO;
             [tble_subCat reloadData];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}




#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_AdssubCategory.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"AdsSubCategory_Cell";
    
    
    //    [tableView setSeparatorInset:UIEdgeInsetsZero];
    tble_subCat.separatorColor=[UIColor clearColor];
    
    AdsSubCategory_Cell *cell = (AdsSubCategory_Cell *)[tble_subCat dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil)
    {
        
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        if(deviceResult.height == 736)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdsSubCategory_Cell_6Plus" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdsSubCategory_Cell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    if (indexPath.row%2==0)
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"ffffff"];
    }
    else
    {
        cell.backgroundColor=[myAppDelegate colorWithHexString:@"fafafa"];
    }
    
    
    cell.lbl_SubName.text=[[arr_AdssubCategory objectAtIndex:indexPath.row]valueForKey:@"title"];
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_subThumb setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_AdssubCategory objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak AdsSubCategory_Cell *weakCell = cell;
    [cell.img_subThumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                          
                                          weakCell.img_subThumb.image = img;
                                          [weakCell setNeedsLayout];
                                          
                                      } failure:nil];
    
    weakCell.img_subThumb.clipsToBounds=YES;
    
    
    return cell;
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Ads_Listing_ViewController *AdsProduct=[[Ads_Listing_ViewController alloc]init];
    
    AdsProduct.str_AdsSubCategoryItem=[[arr_AdssubCategory objectAtIndex:indexPath.row]valueForKey:@"subcategory_id"];
    
    [self.navigationController pushViewController:AdsProduct animated:YES];
}


-(void)viewDidLayoutSubviews
{
    if ([tble_subCat respondsToSelector:@selector(setSeparatorInset:)]) {
        [tble_subCat setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tble_subCat respondsToSelector:@selector(setLayoutMargins:)]) {
        [tble_subCat setLayoutMargins:UIEdgeInsetsZero];
    }
}





#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}

#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate AdvertisementRightNavigation:[sender tag]] animated:YES] ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backtoPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
