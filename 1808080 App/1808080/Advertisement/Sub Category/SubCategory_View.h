//
//  SubCategory_View.h
//  1808080
//
//  Created by RailsBox on 01/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategory_View : UIViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
    
    IBOutlet UIButton *btn_back;
    
    NSMutableArray *arr_AdssubCategory;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tble_subCat;

@property (strong, nonatomic) NSString *str_AdsCategoryID;


@end
