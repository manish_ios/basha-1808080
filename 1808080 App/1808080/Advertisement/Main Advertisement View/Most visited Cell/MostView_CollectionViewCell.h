//
//  MostView_CollectionViewCell.h
//  1808080
//
//  Created by RailsBox on 29/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MostView_CollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img_mostThumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mostName;



@end
