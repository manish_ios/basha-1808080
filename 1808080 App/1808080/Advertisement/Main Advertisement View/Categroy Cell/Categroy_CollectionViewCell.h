//
//  Categroy_CollectionViewCell.h
//  1808080
//
//  Created by RailsBox on 01/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Categroy_CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_categoryLogo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_category;

@end
