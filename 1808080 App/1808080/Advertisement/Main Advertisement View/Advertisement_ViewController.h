//
//  Advertisement_ViewController.h
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Advertisement_ViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *LeftSpeedView;
    
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
    
    IBOutlet UIButton *btn_HomeMenu;
    
    NSMutableArray *arr_category;
    NSMutableArray *arr_mostView;
    NSMutableArray *arr_latest;

    NSMutableArray *arr_details;
}


@property (strong, nonatomic) IBOutlet UICollectionView *collect_latest;
@property (strong, nonatomic) IBOutlet UICollectionView *collect_mostView;
@property (strong, nonatomic) IBOutlet UICollectionView *collect_categories;



@end
