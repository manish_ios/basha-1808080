//
//  Advertisement_ViewController.m
//  1808080
//
//  Created by RailsBox on 28/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Advertisement_ViewController.h"
#import "Latest_CollectionViewCell.h"
#import "MostView_CollectionViewCell.h"
#import "Categroy_CollectionViewCell.h"
#import "SubCategory_View.h"
#import "AdsDetails_ViewController.h"

#import "Ads_Listing_ViewController.h"
#import "AddAds_ViewController.h"

#import "navigator_view.h"
#import "Main_ViewController.h"

#import "AppDelegate.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface Advertisement_ViewController ()
{
    navigator_view *navigate;
}

@end

@implementation Advertisement_ViewController

@synthesize collect_latest,collect_mostView,collect_categories;



static NSString * CellIdentifier = @"CellIdentifier";

static NSString * CellIdentifier2 = @"CellIdentifiers";

static NSString * CellIdentifier3 = @"CellIdentifiers3";


AddAds_ViewController *addads;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"Advertisement_ViewController6Plus";   // iPhone 6+"
        }
        else
        {
            nibNameOrNil = @"Advertisement_ViewController";   // iPhone
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



#pragma mark ViewDidLoad
- (void)viewDidLoad
{
    addads=[[AddAds_ViewController alloc]init];
    
    navigate=[[navigator_view alloc]init];
    
    UINib *cellNib;
    cellNib = [UINib nibWithNibName:@"Latest_CollectionViewCell" bundle:nil];
    [collect_latest registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    
    
    UINib *cellNib2;
    cellNib2 = [UINib nibWithNibName:@"MostView_CollectionViewCell" bundle:nil];
    [collect_mostView registerNib:cellNib2 forCellWithReuseIdentifier:CellIdentifier2];
    
    UINib *cellNib3;
    cellNib3 = [UINib nibWithNibName:@"Categroy_CollectionViewCell" bundle:nil];
    [collect_categories registerNib:cellNib3 forCellWithReuseIdentifier:CellIdentifier3];

    
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_HomeMenu.hidden=NO;
    btn_HomeMenu.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


-(void)viewWillAppear:(BOOL)animated
{
    btn_showRight.tag=1;
    btn_showRight1.tag=0;
    blurView.hidden=YES;
    btn_HomeMenu.hidden=NO;
    btn_HomeMenu.userInteractionEnabled=YES;
    btn_showRight1.hidden=YES;
    speed_view.hidden=YES;
}


-(void)viewDidAppear:(BOOL)animated
{
    [self get_MostViewd];
    [self get_Latest];
    [self get_Category];
}

#pragma mark JSON parsing: get Most Viewed List
-(void)get_MostViewd
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"mosted_ads_views"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_mostView=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayMostViews:%@",arr_mostView);
             
            [collect_mostView reloadData];
             collect_mostView.hidden=NO;
         }
         else
         {
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
     }];
}

#pragma mark JSON parsing: get Latest ads List
-(void)get_Latest
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"latest_ads_items"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_latest=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayLatest:%@",arr_latest);
             
             [collect_latest reloadData];
             collect_latest.hidden=NO;
         }
         else
         {
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
     }];
}


#pragma mark JSON parsing: get Category List
-(void)get_Category
{
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"ads_categories"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_category=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayCategory:%@",arr_category);
             
             myAppDelegate.arr_category_advert=arr_category;
             
             [collect_categories reloadData];
             collect_categories.hidden=NO;
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             NSLog(@"not Found");
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
}




#pragma mark Collection View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if(collectionView==collect_categories)
    {
        return 5;
    }
    else
        return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView==collect_categories)
    {
        return 5;
    }
    else
        return 0;
}


// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView==collect_categories)
    {
        return UIEdgeInsetsMake(0,5,5,5);  // top, left, bottom, right
    }
    else
    return UIEdgeInsetsMake(-1,0,0,0);  // top, left, bottom, right
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if (deviceResult.height==736)
    {
        if (collectionView==collect_categories)
        {
            return CGSizeMake(195, 40);
        }
        else
            return CGSizeMake(107, 150);
    }

    else
    {
        if (collectionView==collect_categories)
        {
            return CGSizeMake(150, 40);
        }
        else
            return CGSizeMake(107, 150);
    }
}

#pragma mark set total elements
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==collect_categories)
    {
        return arr_category.count;
    }
    else if(collectionView==collect_mostView)
    {
        return arr_mostView.count;
    }
    else
        return arr_latest.count;
}

#pragma mark insert elements
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //////set latest collection view 
    if (collectionView==collect_latest)
    {
        Latest_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.layer.borderWidth=0.5;
        cell.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        
        cell.lbl_LatestName.text=[[arr_latest objectAtIndex:indexPath.row] valueForKey:@"title"];
        
        
        CGRect rect = cell.img_LatestThumb.frame;
        
        [cell.img_LatestThumb setFrame:rect];
        
        NSURL *url1 = [NSURL URLWithString:[[arr_latest objectAtIndex:indexPath.row]valueForKey:@"pic"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak Latest_CollectionViewCell *weakCell = cell;
        [cell.img_LatestThumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                               
                                               weakCell.img_LatestThumb.image = img;
                                               [weakCell setNeedsLayout];
                                               
                                           } failure:nil];
        
        weakCell.img_LatestThumb.clipsToBounds=YES;
        
        
        return cell;
    }
    
    
    //////set most category collection view
    else if (collectionView==collect_categories)
    {
        Categroy_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier3 forIndexPath:indexPath];
        
        cell.layer.borderWidth=0.5;
        cell.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        
        cell.lbl_category.text=[[arr_category objectAtIndex:indexPath.row] valueForKey:@"title"];
        
        
        CGRect rect = cell.img_categoryLogo.frame;
        
        [cell.img_categoryLogo setFrame:rect];
        
        NSURL *url1 = [NSURL URLWithString:[[arr_category objectAtIndex:indexPath.row]valueForKey:@"logo"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak Categroy_CollectionViewCell *weakCell = cell;
        [cell.img_categoryLogo setImageWithURLRequest:request placeholderImage:placeholderImage
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                               
                                               weakCell.img_categoryLogo.image = img;
                                               [weakCell setNeedsLayout];
                                               
                                           } failure:nil];
        
        weakCell.img_categoryLogo.clipsToBounds=YES;

        
        
        return cell;
    }
    
    
    //////set most viewed collection view
    else
    {
        MostView_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier2 forIndexPath:indexPath];
        
        cell.layer.borderWidth=0.5;
        cell.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        
        cell.lbl_mostName.text=[[arr_mostView objectAtIndex:indexPath.row] valueForKey:@"title"];
        
        
        CGRect rect = cell.img_mostThumb.frame;
        
        [cell.img_mostThumb setFrame:rect];
        
        NSURL *url1 = [NSURL URLWithString:[[arr_mostView objectAtIndex:indexPath.row]valueForKey:@"pic"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak MostView_CollectionViewCell *weakCell = cell;
        [cell.img_mostThumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                           
                                           weakCell.img_mostThumb.image = img;
                                           [weakCell setNeedsLayout];
                                           
                                       } failure:nil];
        
        weakCell.img_mostThumb.clipsToBounds=YES;

        return cell;
    }
}


#pragma mark collection view didselect
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==collect_categories)
    {
        SubCategory_View *subcategory=[[SubCategory_View alloc]init];
        
        subcategory.str_AdsCategoryID=[[arr_category objectAtIndex:indexPath.row] valueForKey:@"category_id"];
        
        [self.navigationController pushViewController:subcategory animated:YES];
    }
    else if (collectionView==collect_latest)
    {
        [self Get_getdetails:[[arr_latest objectAtIndex:indexPath.row]valueForKey:@"item_id"]];
        
        NSLog(@"%ld",(long)indexPath.row);
    }
    else
    {
        [self Get_getdetails:[[arr_mostView objectAtIndex:indexPath.row]valueForKey:@"item_id"]];

        NSLog(@"%ld",(long)indexPath.row);
    }
}

#pragma mark Navigate to show all latest ads
-(IBAction)ShowAllLatestAds:(id)sender
{
    Ads_Listing_ViewController *showallLatest=[[Ads_Listing_ViewController alloc]init];
    showallLatest.arr_adsListing=arr_latest;
    
    myAppDelegate.str_ads_rightMenu=@"MainLatest";
    
    [self.navigationController pushViewController:showallLatest animated:YES];
}

#pragma mark Navigate to show all MOst Viewed ads
-(IBAction)ShowAllMostAds:(id)sender
{
    Ads_Listing_ViewController *showallMOst=[[Ads_Listing_ViewController alloc]init];
    showallMOst.arr_adsListing=arr_mostView;
    
    myAppDelegate.str_ads_rightMenu=@"MainMost";
    
    [self.navigationController pushViewController:showallMOst animated:YES];
}

#pragma mark JSON parsing to get details
-(void)Get_getdetails:(NSString *)str_detailsID
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_ads_details"]];
    
    
    NSDictionary *parameters = @{@"item_id":str_detailsID};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_details=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArraySubListing:%@",arr_details);
             
             
             AdsDetails_ViewController *adDetail=[[AdsDetails_ViewController alloc]init];
             adDetail.arr_details=arr_details;
             [self.navigationController pushViewController:adDetail animated:YES];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    LeftSpeedView.hidden=YES;
    
    btn_HomeMenu.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_HomeMenu.hidden=NO;
        btn_HomeMenu.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}


-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_HomeMenu.hidden=YES;
    btn_HomeMenu.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}



#pragma mark right menu selection
-(IBAction)LeftMenuSelection:(id)sender
{
    LeftSpeedView.hidden=YES;
    [self.navigationController pushViewController:[navigate navigateToClass:[sender tag]] animated:YES] ;
}


#pragma mark show Left slide view
-(IBAction)showLeftMenu:(id)sender
{
    [self.view addSubview:LeftSpeedView];
    
    
    if ([sender tag]==1)
    {
        LeftSpeedView.hidden=NO;
        
        LeftSpeedView.frame=CGRectMake(LeftSpeedView.frame.origin.x, 64, LeftSpeedView.frame.size.width, LeftSpeedView.frame.size.height);
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=2;
    }
    
    else
    {
        LeftSpeedView.hidden=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[LeftSpeedView layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_HomeMenu.tag=1;
    }
}

#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate AdvertisementRightNavigation:[sender tag]] animated:YES] ;

}


@end
