//
//  Latest_CollectionViewCell.h
//  1808080
//
//  Created by RailsBox on 29/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Latest_CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_LatestThumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_LatestName;

@end
