//
//  PDF_ViewController.h
//  1808080
//
//  Created by RailsBox on 09/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDF_ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *PDFWebView;
@property (strong, nonatomic) NSString *Str_PDF_Url;

@end
