//
//  PDF_ViewController.m
//  1808080
//
//  Created by RailsBox on 09/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "PDF_ViewController.h"

@interface PDF_ViewController ()

@end

@implementation PDF_ViewController

@synthesize PDFWebView,Str_PDF_Url;


- (void)viewDidLoad
{
    NSURL *targetURL = [NSURL URLWithString:Str_PDF_Url];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [PDFWebView loadRequest:request];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)BackToADs:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
