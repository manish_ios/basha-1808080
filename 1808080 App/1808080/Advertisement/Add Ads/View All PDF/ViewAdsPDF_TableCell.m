//
//  ViewAdsPDF_TableCell.m
//  1808080
//
//  Created by RailsBox on 08/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "ViewAdsPDF_TableCell.h"

@implementation ViewAdsPDF_TableCell


@synthesize img_thumb,lbl_Date,lbl_PDFName;
@synthesize lbl_ADSDesc,lbl_newPaperName;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
