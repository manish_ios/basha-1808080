//
//  ViewAdsPDF_TableCell.h
//  1808080
//
//  Created by RailsBox on 08/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewAdsPDF_TableCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img_thumb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PDFName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Date;


@property (strong, nonatomic) IBOutlet UILabel *lbl_newPaperName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ADSDesc;

@end
