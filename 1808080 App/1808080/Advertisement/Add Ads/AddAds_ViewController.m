//
//  AddAds_ViewController.m
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "AddAds_ViewController.h"
#import "ViewAdsPDF_TableCell.h"
#import "PDF_ViewController.h"
#import "Advertisement_ViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#import "navigator_view.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AddAds_ViewController ()
{
    navigator_view *navigate;
    CGSize deviceResult;
}

@end

@implementation AddAds_ViewController

@synthesize tbl_AdsPdf,tbl_allAds;
@synthesize view_AllAds,view_PDf,view_PostAds;
@synthesize segmentAds;
@synthesize str_Segmentstatus;
@synthesize theScrollView_Sell;

@synthesize txt_category,txt_Date,txt_desc,txt_Name,txt_NewspaperType,txt_PhoneNo,txt_Subcategory,img1,img2,img3,btnNewspaper,btnClassified;

BOOL NoApiCall;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"AddAds_ViewController_6Plus";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark ViewDidLoad
- (void)viewDidLoad
{
    NoApiCall=true;
    
    NSLog(@"category_data=%@",myAppDelegate.arr_category_advert);
    navigate=[[navigator_view alloc]init];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoards:)];
    gestureRecognizer.delegate = self;
    
    [theScrollView_Sell addGestureRecognizer:gestureRecognizer];
    
    
    ////////keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShowns:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHides:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    ////////////////
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(refreshTweets) forControlEvents:UIControlEventValueChanged];
    
    [tbl_allAds addSubview:_refreshControl];
    
    self.refreshControl.tintColor = [UIColor colorWithRed:(254/255.0) green:(153/255.0) blue:(0/255.0) alpha:1];
    
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


#pragma mark refresh tableview on pull down
- (void)refreshTweets
{
    [self get_AllADS];
    [self.refreshControl endRefreshing];
}


#pragma mark View Will Appear
-(void)viewWillAppear:(BOOL)animated
{
    
    deviceResult = [[UIScreen mainScreen] bounds].size;
    
    
    frame=CGRectMake(0, 117, self.view.frame.size.width, self.view.frame.size.height-117);
    view_PostAds.frame=frame;
    view_AllAds.frame=frame;
    view_PDf.frame=frame;
    
    
    if (!NoApiCall)
    {
        NSLog(@"No API Calls");
    }
    else
    {
        [self get_AllNewsPaper];
        [self get_AllADS];
        [self get_AdsPDFFIle];
    }
    
    if ([myAppDelegate.str_Segmentstatus isEqualToString:@"AddAds"])
    {
        view_PostAds.hidden=NO;
        view_AllAds.hidden=YES;
        view_PDf.hidden=YES;
        
        [self.view addSubview:view_PostAds];
        segmentAds.selectedSegmentIndex=0;
        myAppDelegate.str_Segmentstatus=@"";
    }
    else if ([myAppDelegate.str_Segmentstatus isEqualToString:@"AllAds"])
    {
        view_PostAds.hidden=YES;
        view_AllAds.hidden=NO;
        view_PDf.hidden=YES;
        
        [self.view addSubview:view_AllAds];
        segmentAds.selectedSegmentIndex=1;
        myAppDelegate.str_Segmentstatus=@"";
    }
    else if ([myAppDelegate.str_Segmentstatus isEqualToString:@"PDF"])
    {
        view_PostAds.hidden=YES;
        view_AllAds.hidden=YES;
        view_PDf.hidden=NO;
        
        [self.view addSubview:view_PDf];
        segmentAds.selectedSegmentIndex=2;
        myAppDelegate.str_Segmentstatus=@"";
    }
    
    
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(hide_picker:)];
    doneBtn.tintColor=[UIColor whiteColor];
    [barItems addObject:doneBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
}

#pragma mark get categroy
-(IBAction)getcategory:(id)sender
{
    [self.view endEditing:YES];
    
    [self hidePicker];
    
    UIPickerView *category_picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0) ];
    
    category_picker.hidden=NO;
    category_picker = [[UIPickerView alloc] init];
    category_picker.showsSelectionIndicator = YES;
    CGRect pickerRect = category_picker.bounds;
    category_picker.bounds = pickerRect;
    
    category_picker.dataSource = self;
    category_picker.delegate = self;
    category_picker.tag=10;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if(deviceResult.height == 568)
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 250, 320, 464)];
            // iPhone Classic
        }
        else if(deviceResult.height==480)
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 150, 320, 464)];
        }
        else
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 350, 414, 464)];
            // iPhone Classic
        }
    }
    
    
    popoverView.backgroundColor = [UIColor whiteColor];
    [popoverView addSubview:category_picker];
    
    [popoverView addSubview:pickerToolbar];
    [self.view_PostAds addSubview:popoverView];
}

#pragma mark get sub Category
-(IBAction)getSubcategroy:(id)sender
{
    [self.view endEditing:YES];
    
    [self hidePicker];
    
    UIPickerView *sub_catgory_picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0) ];
    
    sub_catgory_picker.hidden=NO;
    sub_catgory_picker = [[UIPickerView alloc] init];
    sub_catgory_picker.showsSelectionIndicator = YES;
    CGRect pickerRect = sub_catgory_picker.bounds;
    sub_catgory_picker.bounds = pickerRect;
    
    sub_catgory_picker.dataSource = self;
    sub_catgory_picker.delegate = self;
    sub_catgory_picker.tag=20;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if(deviceResult.height == 568)
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 250, 320, 464)];
            // iPhone Classic
        }
        else if(deviceResult.height==480)
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 150, 320, 464)];
        }
        else
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 350, 414, 464)];
            // iPhone Classic
        }
    }
    
    popoverView.backgroundColor = [UIColor whiteColor];
    [popoverView addSubview:sub_catgory_picker];
    
    [popoverView addSubview:pickerToolbar];
    [self.view_PostAds addSubview:popoverView];
}

#pragma mark get newspaper
-(IBAction)getnewspaper:(id)sender
{
    [self.view endEditing:YES];
    
    [self hidePicker];
    
    UIPickerView *NewsPaper_picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0) ];
    
    NewsPaper_picker.hidden=NO;
    NewsPaper_picker = [[UIPickerView alloc] init];
    NewsPaper_picker.showsSelectionIndicator = YES;
    CGRect pickerRect = NewsPaper_picker.bounds;
    NewsPaper_picker.bounds = pickerRect;
    
    NewsPaper_picker.dataSource = self;
    NewsPaper_picker.delegate = self;
    NewsPaper_picker.tag=30;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if(deviceResult.height == 568)
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 250, 320, 464)];
            // iPhone Classic
        }
        else if(deviceResult.height==480)
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 150, 320, 464)];
        }
        else
        {
            popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 350, 414, 464)];
            // iPhone Classic
        }
    }
    
    popoverView.backgroundColor = [UIColor whiteColor];
    [popoverView addSubview:NewsPaper_picker];
    
    [popoverView addSubview:pickerToolbar];
    [self.view_PostAds addSubview:popoverView];
}


#pragma mark Picker view delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
    
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if(pickerView.tag==10)
    {
        return myAppDelegate.arr_category_advert.count;
    }
    else if (pickerView.tag==20)
    {
        if (arr_subcategory.count==0)
        {
            return 1;
        }
        else
            return arr_subcategory.count;
    }
    else if (pickerView.tag==30)
    {
        return arr_newsPapers.count;
    }
    else
    {
        return 0;
    }
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if(pickerView.tag==10)
    {
        return [[myAppDelegate.arr_category_advert objectAtIndex:row]valueForKey:@"title"] ;
    }
    
    else if (pickerView.tag==20)
    {
        if (arr_subcategory.count==0)
        {
            return @"Please Select Category First";
        }
        else
            return [[arr_subcategory objectAtIndex:row]valueForKey:@"title"];
    }
    
    else if (pickerView.tag==30)
    {
        return [[arr_newsPapers objectAtIndex:row] valueForKey:@"title"];
    }
    
    
    else
        return nil;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if (pickerView.tag==10)
    {
        txt_category.text=[[myAppDelegate.arr_category_advert objectAtIndex:row] valueForKey:@"title"] ;
        
        str_AdsCategoryID=[[myAppDelegate.arr_category_advert objectAtIndex:row] valueForKey:@"category_id"];
        
        [self AdsGet_SubCategory];
    }
    
    else if (pickerView.tag==20)
    {
        txt_Subcategory.text=[[arr_subcategory objectAtIndex:row]valueForKey:@"title"] ;
    }
    
    else if (pickerView.tag==30)
    {
        txt_NewspaperType.text=[[arr_newsPapers objectAtIndex:row] valueForKey:@"title"];
    }
    
    else
    {
        
    }
}




/////hide picker method
#pragma mark Hide picker method
-(IBAction)hide_picker:(id)sender
{
    NSLog(@"Hide_picker");
    popoverView.hidden=YES;
}
-(void)hidePicker
{
    popoverView.hidden=YES;
}



#pragma mark segment Selection
-(IBAction)selectSegment:(id)sender
{
    if (segmentAds.selectedSegmentIndex==0)
    {
        view_PostAds.hidden=NO;
        view_AllAds.hidden=YES;
        view_PDf.hidden=YES;
        
        NSLog(@"Post Ads");
        [self.view addSubview:view_PostAds];
    }
    else if (segmentAds.selectedSegmentIndex==1)
    {
        view_PostAds.hidden=YES;
        view_AllAds.hidden=NO;
        view_PDf.hidden=YES;
        
        //[self get_AllADS];
        
        [self.view addSubview:view_AllAds];
        NSLog(@"View All Ads");
    }
    else
    {
        view_PostAds.hidden=YES;
        view_AllAds.hidden=YES;
        view_PDf.hidden=NO;
        
        // [self get_AdsPDFFIle];
        
        [self.view addSubview:view_PDf];
        NSLog(@"Ads PDf");
    }
}

#pragma mark Call Post Ads Method
-(IBAction)PostAds:(id)sender
{
    
}

#pragma mark JSON parsing Get sub-category
-(void)AdsGet_SubCategory
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_ads_subcategories"]];
    
    NSDictionary *parameters = @{@"categories_id":str_AdsCategoryID};
    NSLog(@"parameters %@",parameters);
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_subcategory=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"arrayAdsSubCategroy:%@",arr_subcategory);
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}



#pragma mark JSON parsing: get All AD'S PDF
-(void)get_AdsPDFFIle
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_all_newspaper"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_AdsPDF=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayALLPDF:%@",arr_AdsPDF);
             
             [tbl_AdsPdf reloadData];
             tbl_AdsPdf.hidden=NO;
             
             [MBProgressHUD hideHUDForView:view_PostAds animated:YES];
         }
         else
         {
             NSLog(@"not Found");
             [MBProgressHUD hideHUDForView:view_PostAds animated:YES];
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:view_PostAds animated:YES];
         
     }];
}


#pragma mark JSON parsing: get All AD'S
-(void)get_AllADS
{
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_all_adervertisment"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_AllADS=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayALLADS:%@",arr_AllADS);
             
             [tbl_allAds reloadData];
             tbl_allAds.hidden=NO;
         }
         else
         {
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
     }];
}

#pragma mark JSON parsing: get All NewsPaper
-(void)get_AllNewsPaper
{
    [MBProgressHUD showHUDAddedTo:view_PostAds animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"get_all_newspaper"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_newsPapers=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayAllNewsPapers:%@",arr_newsPapers);
         }
         else
         {
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
     }];
}


/*
 #pragma mark JSON parsing to post ads
 -(void)Get_PostAds
 {
 [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
 NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"add_advertiesment"]];
 
 
 NSDictionary *parameters = @{@"newspaper_id":txt_PaperCode.text,@"description":txt_desc.text,@"phone":txt_MobileNo.text,@"Date":txt_Date.text,@"location":txt_Location.text};
 NSLog(@"parameters %@",parameters);
 
 
 // 2
 AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
 
 manager.responseSerializer = [AFJSONResponseSerializer serializer];
 manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
 
 [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
 {
 NSLog(@"Response object %@",responseObject);
 NSArray *responseArr=[responseObject valueForKey:@"Response"];
 
 if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
 {
 NSLog(@"ArraySubPostAds:%@",responseArr);
 txt_Date.text=@"";
 txt_desc.text=@"";
 txt_Location.text=@"";
 txt_MobileNo.text=@"";
 txt_newPaperName.text=@"";
 txt_PaperCode.text=@"";
 
 [MBProgressHUD hideHUDForView:self.view animated:YES];
 }
 else
 {
 [MBProgressHUD hideHUDForView:self.view animated:YES];
 }
 }
 failure:^(NSURLSessionDataTask *task, NSError *error)
 {
 [MBProgressHUD hideHUDForView:self.view animated:YES];
 NSLog(@"Error : %@",error);
 }];
 }
 */

#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tbl_AdsPdf)
    {
        return 72;
    }
    
    else
        return 82;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tbl_AdsPdf)
    {
        return arr_AdsPDF.count;        ///PDF ARRAY COUNT
    }
    else
        return arr_AllADS.count;        ///ADVERTISEMENT COUNT
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tbl_AdsPdf.separatorColor=[UIColor clearColor];
    tbl_allAds.separatorColor=[UIColor clearColor];
    
    
    if (tableView == tbl_AdsPdf)
    {
        NSString *identifier=@"ViewAdsPDF_TableCell";
        
        ViewAdsPDF_TableCell *cell = (ViewAdsPDF_TableCell *)[tbl_AdsPdf dequeueReusableCellWithIdentifier:identifier];
        
        
        if (cell == nil)
        {
            if(deviceResult.height == 736)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ViewAdsPDF_TableCell_6Plus" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            else
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ViewAdsPDF_TableCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
        }
        
        if (indexPath.row%2==0)
        {
            cell.backgroundColor=[myAppDelegate colorWithHexString:@"ffffff"];
        }
        else
        {
            cell.backgroundColor=[myAppDelegate colorWithHexString:@"fafafa"];
        }
        
        
        cell.lbl_PDFName.text=[[arr_AdsPDF objectAtIndex:indexPath.row]valueForKey:@"newspaper_name"];
        
        cell.lbl_Date.text=[[arr_AdsPDF objectAtIndex:indexPath.row]valueForKey:@"created"];
        
        CGRect rect = cell.imageView.frame;
        
        [cell.img_thumb setFrame:rect];
        
        NSURL *url1 = [NSURL URLWithString:[[arr_AdsPDF objectAtIndex:indexPath.row]valueForKey:@"pic"]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url1];
        UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
        
        
        __weak ViewAdsPDF_TableCell *weakCell = cell;
        [cell.img_thumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                           
                                           weakCell.img_thumb.image = img;
                                           [weakCell setNeedsLayout];
                                           
                                       } failure:nil];
        
        weakCell.img_thumb.clipsToBounds=YES;
        
        
        return cell;
    }
    
    
    else  //// view all ads
    {
        NSString *identifier=@"ViewAdsPDF_TableCellAllAds";
        
        ViewAdsPDF_TableCell *cell = (ViewAdsPDF_TableCell *)[tbl_allAds dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil)
        {
            if(deviceResult.height == 736)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ViewAdsPDF_TableCellAllAds_6Plus" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            else
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ViewAdsPDF_TableCellAllAds" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
        }
        
        if (indexPath.row%2==0)
        {
            cell.backgroundColor=[myAppDelegate colorWithHexString:@"ffffff"];
        }
        else
        {
            cell.backgroundColor=[myAppDelegate colorWithHexString:@"fafafa"];
        }
        
        
        cell.lbl_newPaperName.text=[[arr_AllADS objectAtIndex:indexPath.row]valueForKey:@"title"];
        
        //cell.lbl_newPaperName.text=@"Advertisement names";
        
        cell.lbl_ADSDesc.text=[[arr_AllADS objectAtIndex:indexPath.row]valueForKey:@"description"];
        return cell;
    }
}

#pragma mark tableview cell select
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
    if(tableView == tbl_AdsPdf)
    {
        PDF_ViewController *pdf_viewer=[[PDF_ViewController alloc]init];
        pdf_viewer.Str_PDF_Url=[[arr_AdsPDF objectAtIndex:indexPath.row] valueForKey:@"newspaper_pdf"];
        [self.navigationController pushViewController:pdf_viewer animated:YES];
    }
}

/*
 #pragma mark UIGestureRecognizerDelegate methods
 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
 {
 if ([touch.view isDescendantOfView:tbl_newspaper])
 {
 return NO;
 }
 return YES;
 }
 */


#pragma mark Method: date picker delegates
//////////datepicker
-(IBAction)btn_datepicker:(id)sender
{
    btn_Dobpicker.enabled=NO;
    
    [self hidePicker];
    
    [self.view endEditing:YES];
    
    if(deviceResult.height==480)
    {
        dateView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view_PostAds.frame.size.height/2, 320, 480)];
    }
    else
    {
        dateView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view_PostAds.frame.size.height/2+45, 320, 640)];
    }
    
    
    pickerToolbar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0,20, 320, 44)];
    pickerToolbar1.barStyle=UIBarStyleBlackOpaque;
    
    [pickerToolbar1 sizeToFit];
    NSMutableArray *barItems1 = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [barItems1 addObject:flexSpace];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dateChosen:)];
    
    barButtonItem.tintColor=[UIColor whiteColor];
    [barItems1 addObject:barButtonItem];
    barButtonItem.tag = 123;
    
    [pickerToolbar1 setItems:barItems1 animated:YES];
    [dateView addSubview:pickerToolbar1];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,60, 320, 300)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    // datePicker.tag=[sender tag];
    
    [datePicker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    
    datePicker.backgroundColor = [UIColor whiteColor];
    [dateView addSubview:datePicker];
    
    [self.view_PostAds addSubview:dateView];
}

#pragma mark select date
-(void) dateChosen:(UIBarButtonItem *) barButton
{
    NSLog(@"dateChosen:%@",txt_Date.text);
    btn_Dobpicker.enabled=YES;
    
    if(barButton.tag ==123)
    {
        [dateView removeFromSuperview];
    }
    else
    {
        [dateView removeFromSuperview];
    }
}


#pragma mark change date
-(void)dueDateChanged:(id)sender
{
    NSLog(@"senderTag:%ld",(long)[sender tag]);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    txt_Date.text=[dateFormatter stringFromDate:[sender date]];
    
    NSLog(@"dateis:%@",[dateFormatter stringFromDate:[sender date]]);
}


#pragma mark set cell saperator size
-(void)viewDidLayoutSubviews
{
    if ([tbl_AdsPdf respondsToSelector:@selector(setSeparatorInset:)] || [tbl_allAds respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tbl_AdsPdf setSeparatorInset:UIEdgeInsetsZero];
        [tbl_allAds setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tbl_AdsPdf respondsToSelector:@selector(setLayoutMargins:)] || [tbl_allAds respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tbl_AdsPdf setLayoutMargins:UIEdgeInsetsZero];
        [tbl_allAds setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark button advertise type as newspaper
-(IBAction)AdsType_newspaper:(id)sender
{
    if ([sender tag]==0)
    {
        newpaperTypeStatus=@"1";
        [btnNewspaper setImage:[UIImage imageNamed:@"detailcheckboxchecked.png"] forState:UIControlStateNormal];
        btnNewspaper.tag=1;
    }
    else
    {
        newpaperTypeStatus=@"0";
        [btnNewspaper setImage:[UIImage imageNamed:@"detailcheckbox.png"] forState:UIControlStateNormal];
        btnNewspaper.tag=0;
    }
}


#pragma mark button advertise type as classified
-(IBAction)AdsType_classified:(id)sender
{
    if ([sender tag]==0)
    {
        classifiedStatus=@"1";
        [btnClassified setImage:[UIImage imageNamed:@"detailcheckboxchecked.png"] forState:UIControlStateNormal];
        btnClassified.tag=1;
    }
    else
    {
        classifiedStatus=@"0";
        [btnClassified setImage:[UIImage imageNamed:@"detailcheckbox.png"] forState:UIControlStateNormal];
        btnClassified.tag=0;
    }
}

#pragma mark method addImage
-(IBAction)addImage:(id)sender
{
    pos=[sender tag];
    
    actionsheet = [[UIActionSheet alloc]initWithTitle:@"Select Image Source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Image Gallery", nil];
    actionsheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    actionsheet.alpha = 1.0;
    actionsheet.tag=1;
    [actionsheet showInView:self.view];
}

#pragma mark method action sheet
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    {
        switch (actionsheet.tag) {
            case 1:{
                switch (buttonIndex) {
                    case 0:{
                        
                        
                        ///
#if TARGET_IPHONE_SIMULATOR
                        
                        
                        UIAlertView*  alert=[[UIAlertView alloc] initWithTitle:@"Saw Them" message:@"Camera not available." delegate:nil cancelButtonTitle:@"Alert_cancel" otherButtonTitles:nil];
                        [alert show];
                        
#elif TARGET_OS_IPHONE
                        
                        controller = [[UIImagePickerController alloc] init];
                        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
                        controller.allowsEditing=YES;
                        controller.delegate = self;
                        [self presentViewController:controller animated:YES completion:nil];
                        
#endif
                    }
                        break;
                    case 1:
                    {
                        controller = [[UIImagePickerController alloc]init];
                        controller.allowsEditing=YES;
                        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                        controller.delegate=self;
                        [self presentViewController:controller animated:YES completion:nil];
                    }
                        break;
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }
    }
}

#pragma mark Image picker controller
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    counter=pos;
    
    NoApiCall=false;
    
    if(counter==1)
    {
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        img1.image = chosenImage;
        img_data = [NSData dataWithData:UIImageJPEGRepresentation(chosenImage,0.1f)];
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    if(counter==2)
    {
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        img2.image = chosenImage;
        img_data = [NSData dataWithData:UIImageJPEGRepresentation(chosenImage,0.1f)];
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    if(counter==3)
    {
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        img3.image = chosenImage;
        img_data = [NSData dataWithData:UIImageJPEGRepresentation(chosenImage,0.1f)];
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    // [controller dismissModalViewControllerAnimated:YES];
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    //    [controller dismissModalViewControllerAnimated:YES];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)RemoveImage:(id)sender
{
    img_data=nil;
    
    if ([sender tag]==1)
    {
        img1.image=[UIImage imageNamed:@"Detailimg.png"];
    }
    else if ([sender tag]==2)
    {
        img2.image=[UIImage imageNamed:@"Detailimg.png"];
    }
    else if ([sender tag]==3)
    {
        img3.image=[UIImage imageNamed:@"Detailimg.png"];
    }
}


#pragma mark Upload Ads Button
-(IBAction)Upload_Ads:(id)sender
{
    if ([txt_category.text isEqualToString:@""])
    {
        [self showalerts:@"Please Select Category"];
    }
    else if ([txt_Subcategory.text isEqualToString:@""])
    {
        [self showalerts:@"Please Select Sub-Category"];
    }
    else if ([txt_NewspaperType.text isEqualToString:@""])
    {
        [self showalerts:@"Please Select NewPaper"];
    }
    else if ([txt_Name.text isEqualToString:@""])
    {
        [self showalerts:@"Please Enter Name"];
    }
    
    else if ([txt_PhoneNo.text isEqualToString:@""])
    {
        [self showalerts:@"Please Enter Phone Number"];
    }
    
    else if ([txt_Date.text isEqualToString:@""])
    {
        [self showalerts:@"Please Enter Select Date"];
    }
    else if ([txt_desc.text isEqualToString:@""])
    {
        [self showalerts:@"Please Enter Description"];
    }
    else
    {
        [self API_Add_Advertise];
    }
}

-(void)showalerts:(NSString *)alrtMsg
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:alrtMsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

#pragma mark JSON parsing: Advertise with Us
-(void)API_Add_Advertise
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"add_advertiesment"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
    
    ///////////////////////////Category
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"category\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[txt_category.text dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////////////////////////////////////////////Sub Category
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"subcategory\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txt_Subcategory.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////////////////////////////////////////////NewPaper Type
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"newspapertype\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txt_NewspaperType.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    ////////////////////////////////////////////////////Name
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"name\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txt_Name.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    ////////////////////////////////////////////////////Phone
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"phone\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txt_PhoneNo.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    ////////////////////////////////////////////////////Date
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"date\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txt_Date.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    ////////////////////////////////////////////////////description
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"description\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[txt_desc.text stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    ////////////////////////////////////////////////////Image 1
    if (img_data)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"userfile1\"; filename=\"ipodfile.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:img_data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:img_data];
    }
    
    ////////////////////////////////////////////////////Image 2
    if (img_data)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"userfile2\"; filename=\"ipodfile.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:img_data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:img_data];
    }
    
    ////////////////////////////////////////////////////Image 3
    if (img_data)
    {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"userfile3\"; filename=\"ipodfile.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:img_data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:img_data];
    }
    
    if(newpaperTypeStatus.length==0)
    {
            newpaperTypeStatus=@"0";
    }
    
    ////////////////////////////////////////////////////Newspaper
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"newspaper\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[newpaperTypeStatus stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    if (classifiedStatus.length==0)
    {
        classifiedStatus=@"0";
    }
    ////////////////////////////////////////////////////classified
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"classified\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[classifiedStatus stringByReplacingOccurrencesOfString:@"\n" withString:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    /////////////////////////submit////
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = nil;
    receivedData = [[NSMutableData alloc] init];
    
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString*  text = [[NSString alloc] initWithData:receivedData encoding: NSUTF8StringEncoding];
    
    NSArray  *arr1 = [NSJSONSerialization JSONObjectWithData:[text dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    NSArray *res=[arr1 valueForKey:@"Response"];
    NSLog(@"Ad response %@",res);
    
    if([[[res objectAtIndex:0] valueForKey:@"success"] isEqual:@"S"])
    {
        [self backtoPrevious:self];
    }
    else
    {
        
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    NSLog(@"Error %@",error);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
}





#pragma mark text view delegates
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    popoverView.hidden=YES;
    
    if ([txt_desc.text isEqualToString:@"Description"])
    {
        txt_desc.text = @"";
        txt_desc.textColor = [UIColor darkGrayColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if(txt_desc.text.length==0)
    {
        txt_desc.text = @"Description";
        txt_desc.textColor = [UIColor lightGrayColor];
        [txt_desc resignFirstResponder];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_Name resignFirstResponder];
    [txt_PhoneNo resignFirstResponder];
    [txt_desc resignFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txt_Name resignFirstResponder];
    [txt_PhoneNo resignFirstResponder];
    [txt_desc resignFirstResponder];
    
    return YES;
}


-(IBAction)backtoPrevious:(id)sender
{
    [view_PostAds endEditing:YES];
    
    Advertisement_ViewController *adsMain=[[Advertisement_ViewController alloc]init];
    [self.navigationController pushViewController:adsMain animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}

#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate AdvertisementRightNavigation:[sender tag]] animated:YES] ;
    
}

#pragma mark method to hide keyboard using UITapGestureRecognizer
-(void) :(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    theScrollView_Sell = nil;
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    theScrollView_Sell = nil;
    [super viewDidUnload];
}

/////////////////////////
-(void) hideKeyBoards:(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)keyboardWasShowns:(NSNotification *)notification
{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if(deviceResult.height == 480)
        {
            UIEdgeInsets contentInsets = UIEdgeInsetsMake(-35, 0.0, keyboardSize.height, 0.0);
            theScrollView_Sell.contentInset = contentInsets;
            theScrollView_Sell.scrollIndicatorInsets = contentInsets;
            
            // Step 3: Scroll the target text field into view.
            CGRect aRect = self.view.frame;
            aRect.size.height -= keyboardSize.height;
            if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) )
            {
                CGPoint scrollPoint = CGPointMake(0.0, activeTextField.frame.origin.y - (keyboardSize.height));
                [theScrollView_Sell setContentOffset:scrollPoint animated:YES];
            }
        }
        
        if(deviceResult.height == 568)
        {                // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
            UIEdgeInsets contentInsets = UIEdgeInsetsMake(-10, 0.0, keyboardSize.height, 0.0);
            theScrollView_Sell.contentInset = contentInsets;
            theScrollView_Sell.scrollIndicatorInsets = contentInsets;
            
            // Step 3: Scroll the target text field into view.
            CGRect aRect = self.view.frame;
            aRect.size.height -= keyboardSize.height;
            if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) )
            {
                CGPoint scrollPoint = CGPointMake(0.0, activeTextField.frame.origin.y - (keyboardSize.height-15));
                [theScrollView_Sell setContentOffset:scrollPoint animated:YES];
            }
        }
    }
    [UIView commitAnimations];
}


- (void) keyboardWillHides:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    theScrollView_Sell.contentInset = contentInsets;
    theScrollView_Sell.scrollIndicatorInsets = contentInsets;
}



// Dismiss the keyboard

- (IBAction)dismissKeyboards:(id)sender
{
    [activeTextField resignFirstResponder];
}


@end
