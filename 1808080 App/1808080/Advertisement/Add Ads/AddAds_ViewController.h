//
//  AddAds_ViewController.h
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAds_ViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    CGRect frame;
    
    UITextField *activeTextField;

    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;


    
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
    
    IBOutlet UIButton *btn_back;

    NSMutableArray *arr_AdsPDF;
    NSMutableArray *arr_AllADS;
    NSMutableArray *arr_newsPapers;
    NSMutableArray *arr_subcategory;
    
    ///// Date Picker
    UIToolbar *pickerToolbar1;
    UIActionSheet *pickerViewPopup1;
    UIDatePicker *datePicker;
    UIView* dateView;
    NSString *seleted_date;
    IBOutlet UIButton *btn_Dobpicker;

    UIToolbar *pickerToolbar;
    UIView* popoverView;
    NSMutableArray *barItems;
    
    NSString *str_AdsCategoryID;
    
    
    //image picker
    UIImagePickerController *controller;
    IBOutlet UIImageView *imgView;
    NSData *img_data;
    UIActionSheet *actionsheet;
    int counter;
    NSData *imageData1,*imageData2,*imageData3;
    NSMutableArray *images;
    NSArray *imagesName;
    NSInteger pos;

    NSMutableData *receivedData;
    NSString *newpaperTypeStatus;
    NSString *classifiedStatus;
}

@property (strong, nonatomic) NSString *str_Segmentstatus;
@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView_Sell;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentAds;
@property (strong, nonatomic) IBOutlet UITableView *tbl_allAds;
@property (strong, nonatomic) IBOutlet UITableView *tbl_AdsPdf;

@property (strong, nonatomic) IBOutlet UIButton *btn_newspaper;

@property (strong, nonatomic) IBOutlet UIView *view_PostAds;
@property (strong, nonatomic) IBOutlet UIView *view_PDf;
@property (strong, nonatomic) IBOutlet UIView *view_AllAds;

@property (strong, nonatomic) UIRefreshControl *refreshControl;


///post ads objects

@property (strong, nonatomic) IBOutlet UITextField *txt_category;
@property (strong, nonatomic) IBOutlet UITextField *txt_Subcategory;
@property (strong, nonatomic) IBOutlet UITextField *txt_NewspaperType;
@property (strong, nonatomic) IBOutlet UITextField *txt_Name;
@property (strong, nonatomic) IBOutlet UITextField *txt_Date;
@property (strong, nonatomic) IBOutlet UITextField *txt_PhoneNo;
@property (strong, nonatomic) IBOutlet UITextView  *txt_desc;

@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;

@property (strong, nonatomic) IBOutlet UIButton *btnNewspaper;
@property (strong, nonatomic) IBOutlet UIButton *btnClassified;
@property (strong, nonatomic) IBOutlet UIButton *btnRemoveImg;




@end
