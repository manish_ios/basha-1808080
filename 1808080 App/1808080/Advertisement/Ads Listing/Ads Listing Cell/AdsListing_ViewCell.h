//
//  AdsListing_ViewCell.h
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdsListing_ViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UILabel *lbl_adName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_adDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_adLikes;
@property (strong, nonatomic) IBOutlet UIImageView *imgAdsThumb;



@end
