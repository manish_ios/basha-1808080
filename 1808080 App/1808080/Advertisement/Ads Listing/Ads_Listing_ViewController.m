//
//  Ads_Listing_ViewController.m
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "Ads_Listing_ViewController.h"
#import "AdsListing_ViewCell.h"

#import "Advertisement_ViewController.h"
#import "AdsDetails_ViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

#import "AppDelegate.h"
#import "MBProgressHUD.h"

#import "navigator_view.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface Ads_Listing_ViewController ()
{
    navigator_view *navigate;
}

@end

@implementation Ads_Listing_ViewController

@synthesize collectionAds;

@synthesize str_AdsSubCategoryItem,arr_adsListing;


static NSString * CellIdentifier = @"CellIdentifier";

static NSString * CellIdentifier2 = @"CellIdentifiers";



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"Ads_Listing_ViewController_6Plus";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    navigate=[[navigator_view alloc]init];
    
    
    ///register nib as per devices
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 736)
    {
        UINib *cellNib;
        cellNib = [UINib nibWithNibName:@"AdsListing_ViewCell_6Plus" bundle:nil];
        [collectionAds registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    }
    else
    {
        UINib *cellNib;
        cellNib = [UINib nibWithNibName:@"AdsListing_ViewCell" bundle:nil];
        [collectionAds registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
    }
    
    
    if(str_AdsSubCategoryItem.length>0)
    {
        Lbl_headerText.text=@"Sub Category";
        [self Get_subAdsCategoryListing];
    }
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"latest_Ads"])
    {
        Lbl_headerText.text=@"Latest Ads";
        [self get_All_listing:@"latest_ads_items"];
    }
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"Most_Ads"])
    {
        Lbl_headerText.text=@"Most of Ads View";
        [self get_All_listing:@"mosted_ads_views"];
    }
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"AdsToday"])
    {
        Lbl_headerText.text=@"Today Ads";
        [self get_All_listing:@"get_ads_favoirtes"];
        
    }
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"AdsWeekly"])
    {
        Lbl_headerText.text=@"Weekly Ads";
        [self get_All_listing:@"get_ads_weekly"];
        
    }
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"AdsFavorite"])
    {
        Lbl_headerText.text=@"Favorites";
        [self get_All_listing:@"get_ads_favoirtes"];
    }
    
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"MainLatest"])
    {
        Lbl_headerText.text=@"Latest Ads";
    }
    else if ([myAppDelegate.str_ads_rightMenu isEqualToString:@"MainMost"])
    {
        Lbl_headerText.text=@"Most of Ads View";
    }
    
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


#pragma mark JSON parsing
-(void)Get_subAdsCategoryListing
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_ads_items"]];
    
    
    NSDictionary *parameters = @{@"subcategory_id":str_AdsSubCategoryItem};
    NSLog(@"parameters %@",parameters);
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_adsListing=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArraySubListing:%@",arr_adsListing);
             
             collectionAds.hidden=NO;
             [collectionAds reloadData];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}
#pragma mark Get All Listing
-(void)get_All_listing:(NSString *)StrUrl;
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, StrUrl]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_adsListing=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArrayMostViewd:%@",arr_adsListing);
             
             collectionAds.hidden=NO;
             [collectionAds reloadData];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             NSLog(@"not Found");
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];
}




#pragma mark Collection View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}


// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);  // top, left, bottom, right
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    
    if (deviceResult.height==736)           //iphone 6 plus
    {
        return CGSizeMake(200, 200);
    }
    else
    {
        return CGSizeMake(153, 160);
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arr_adsListing.count;
}

#pragma mark collection view cell for index
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    AdsListing_ViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.layer.borderWidth=0.5;
    cell.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    
    cell.lbl_adName.text=[[arr_adsListing objectAtIndex:indexPath.row]valueForKey:@"title"];
    cell.lbl_adLikes.text=[[arr_adsListing objectAtIndex:indexPath.row]valueForKey:@"likes"];
    cell.lbl_adDate.text=[[arr_adsListing objectAtIndex:indexPath.row]valueForKey:@"created"];
    
    
    CGRect rect = cell.imgAdsThumb.frame;
    
    [cell.imgAdsThumb setFrame:rect];
    
    NSURL *url1 = [NSURL URLWithString:[[arr_adsListing objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak AdsListing_ViewCell *weakCell = cell;
    [cell.imgAdsThumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                         
                                         weakCell.imgAdsThumb.image = img;
                                         [weakCell setNeedsLayout];
                                         
                                     } failure:nil];
    
    weakCell.imgAdsThumb.clipsToBounds=YES;
    
    return cell;
}


#pragma mark collection view didselect
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
    
    [self Get_getdetails:[[arr_adsListing objectAtIndex:indexPath.row]valueForKey:@"item_id"]];
}

#pragma mark JSON parsing to get details
-(void)Get_getdetails:(NSString *)str_detailsID
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_ads_details"]];
    
    NSDictionary *parameters = @{@"item_id":str_detailsID};
    NSLog(@"parameters %@",parameters);
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_details=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArraySubListing:%@",arr_details);
             
             
             AdsDetails_ViewController *adDetail=[[AdsDetails_ViewController alloc]init];
             adDetail.arr_details=arr_details;
             [self.navigationController pushViewController:adDetail animated:YES];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}



#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate AdvertisementRightNavigation:[sender tag]] animated:YES] ;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark navigate to previous
-(IBAction)backtoPrevious:(id)sender
{
    myAppDelegate.str_ads_rightMenu=@"";
    
    Advertisement_ViewController *ads=[[Advertisement_ViewController alloc]init];
    [self.navigationController pushViewController:ads animated:YES];
}
@end
