//
//  Ads_Listing_ViewController.h
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ads_Listing_ViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;
    
    IBOutlet UIButton *btn_back;
    
    NSMutableArray *arr_details;

    IBOutlet UILabel *Lbl_headerText;
    
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionAds;

@property (strong, nonatomic) NSString *str_AdsSubCategoryItem;

@property (strong, nonatomic) NSMutableArray *arr_adsListing;


@end
