//
//  Ads_Deals_ViewController.h
//  1808080
//
//  Created by Manish Gupta on 21/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ads_Deals_ViewController : UIViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
    
    NSMutableArray *arr_ads_deals;
}


@property (strong, nonatomic) IBOutlet UICollectionView *collection_ads_deals;

@end

