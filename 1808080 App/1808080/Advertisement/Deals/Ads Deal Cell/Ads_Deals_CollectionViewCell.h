//
//  Ads_Deals_CollectionViewCell.h
//  1808080
//
//  Created by Manish Gupta on 21/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ads_Deals_CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UIImageView *img_deal;
@property (strong, nonatomic) IBOutlet UILabel *lbl_contact;

@property (strong, nonatomic) IBOutlet UIButton *btn_call;

@end
