//
//  AdsDetails_ViewController.m
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "AdsDetails_ViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#import <Social/Social.h>



#import "navigator_view.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AdsDetails_ViewController ()
{
    navigator_view *navigate;
}

@end

@implementation AdsDetails_ViewController
@synthesize str_detailsID;
@synthesize arr_details,img_header;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"AdsDetails_ViewController_6Plus";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    navigate=[[navigator_view alloc]init];
    
    btnLike.layer.borderColor=[[UIColor redColor]CGColor];
    btnLike.layer.borderWidth=1;
    btnLike.layer.cornerRadius=3;
    
    [self showDetails];
    
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"ArraySubListing:%@",arr_details);
}

#pragma mark JSON parsing
-(void)Get_getdetails
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"find_ads_details"]];
    
    NSDictionary *parameters = @{@"item_id":str_detailsID};
    NSLog(@"parameters %@",parameters);
    
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"Response object %@",responseObject);
         NSArray *responseArr=[responseObject valueForKey:@"Response"];
         
         if ([[[responseArr objectAtIndex:0]valueForKey:@"success"]isEqualToString:@"S"])
         {
             arr_details=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"ArraySubListing:%@",arr_details);
             
             [self showDetails];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Error : %@",error);
     }];
    
}


#pragma mark show details
-(void)showDetails
{
    lbl_headerTitle.text=[[arr_details objectAtIndex:0]valueForKey:@"title"];
    lbl_title.text=[[arr_details objectAtIndex:0]valueForKey:@"title"];
    lbl_dateCreated.text=[[arr_details objectAtIndex:0]valueForKey:@"created"];
    text_desc.text=[[arr_details objectAtIndex:0]valueForKey:@"description"];
    lbl_likes.text=[[arr_details objectAtIndex:0]valueForKey:@"likes"];
    
    [btnLike setTitle:[[[arr_details objectAtIndex:0]valueForKey:@"likes"] stringByAppendingString:@" Likes"] forState:UIControlStateNormal];
    
    NSURL *url = [NSURL URLWithString:[[arr_details objectAtIndex:0]valueForKey:@"pic"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    UIImage *placeholderImage = [UIImage imageNamed:@"Detailimg.png"];
    [img_header setImageWithURLRequest:request
                      placeholderImage:placeholderImage
                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                   
                                   img_header.image = image;
                                   
                               } failure:nil];
}

#pragma mark Make a call
-(IBAction)DetailsButtons:(id)sender
{
    if ([sender tag]==1)    ////make call
    {
        NSLog(@"Make Call");
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"teleprompt://%@",[[arr_details objectAtIndex:0]valueForKey:@"phone"]]];
        
        [[UIApplication  sharedApplication] openURL:url];
    }
    
    else if ([sender tag]==2)       ////send sms
    {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"sms://%@",[[arr_details objectAtIndex:0]valueForKey:@"phone"]]];
        
        [[UIApplication sharedApplication]openURL:url];
    }
    
    else if ([sender tag]==3)       ///share on fb
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            
            SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            [mySLComposerSheet setInitialText:[[arr_details objectAtIndex:0] valueForKey:@"description"]];
            
            NSURL *url = [NSURL URLWithString:[[arr_details objectAtIndex:0]valueForKey:@"pic"]];
            NSData *data=[NSData dataWithContentsOfURL:url];
            
            
            [mySLComposerSheet addImage:[UIImage imageWithData:data]];
            
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"Post Canceled");
                        break;
                    case SLComposeViewControllerResultDone:
                        NSLog(@"Post Sucessful");
                        break;
                        
                    default:
                        break;
                }
            }];
            
            [self presentViewController:mySLComposerSheet animated:YES completion:nil];
        }
    }
    
    else if ([sender tag]==4)       ////share on twitter
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            
            [mySLComposerSheet setInitialText:[[arr_details objectAtIndex:0] valueForKey:@"description"]];
            
            NSURL *url = [NSURL URLWithString:[[arr_details objectAtIndex:0]valueForKey:@"pic"]];
            NSData *data=[NSData dataWithContentsOfURL:url];
            
            
            [mySLComposerSheet addImage:[UIImage imageWithData:data]];
            
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"Post Canceled");
                        break;
                    case SLComposeViewControllerResultDone:
                        NSLog(@"Post Sucessful");
                        break;
                        
                    default:
                        break;
                }
            }];
            [self presentViewController:mySLComposerSheet animated:YES completion:nil];
        }
    }
    
    else if ([sender tag]==5)       //// share on wats up
    {
        [self share_data];
    }
    
    else if ([sender tag]==6)       //// share on google+
    {
        [self share_data];
    }
    
    
    else if ([sender tag]==7)       ///send mail
    {
        NSLog(@"send mail");
        // Email Content
        NSString *messageBody =[[arr_details objectAtIndex:0]valueForKey:@"description"];
        
        // To address
        //NSArray *toRecipents = [arr_details valueForKey:@"email"];
        NSArray *toRecipents = [NSArray arrayWithObject:[[arr_details objectAtIndex:0]valueForKey:@"email"]];
        
        NSLog(@"toRecipents %@ ",toRecipents);
        
        MFMailComposeViewController *msg = [[MFMailComposeViewController alloc] init];
        
        NSURL *url = [NSURL URLWithString:[[arr_details objectAtIndex:0]valueForKey:@"pic"]];
        NSData *data=[NSData dataWithContentsOfURL:url];
        
        [msg addAttachmentData:data mimeType:@"image/jpeg" fileName:@"Picture.jpeg"];
        
        msg.mailComposeDelegate = self;
        [msg setSubject:@"Share App Information"];
        [msg setMessageBody:messageBody isHTML:NO];
        [msg setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:msg animated:YES completion:NULL];
    }
}

#pragma mark Share Data methos
-(void)share_data
{
    NSString *texttoshare = [[arr_details objectAtIndex:0] valueForKey:@"description"]; //this is your text string to share
    
    NSURL *url = [NSURL URLWithString:[[arr_details objectAtIndex:0]valueForKey:@"pic"]];
    NSData *data=[NSData dataWithContentsOfURL:url];
    
    NSArray *activityItems = @[texttoshare,data];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

#pragma mark email composer delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}






#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}


#pragma mark Navigate to main view
-(IBAction)RightNavigateControl:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate AdvertisementRightNavigation:[sender tag]] animated:YES] ;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backtoPrevious:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
