//
//  AdsDetails_ViewController.h
//  1808080
//
//  Created by Manish Gupta on 31/05/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MessageUI.h>


@interface AdsDetails_ViewController : UIViewController<MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIButton *btnLike;
    
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    IBOutlet UIButton *btn_showRight;
    IBOutlet UIButton *btn_showRight1;
    IBOutlet UIButton *btn_back;
    
    ////details data
    IBOutlet UILabel *lbl_title;
    IBOutlet UILabel *lbl_dateCreated;
    IBOutlet UITextView *text_desc;
    IBOutlet UILabel *lbl_likes;
    
    IBOutlet UILabel *lbl_headerTitle;
}

@property (strong, nonatomic) NSString *str_detailsID;
@property (strong, nonatomic) NSMutableArray *arr_details;

@property(strong, nonatomic) IBOutlet UIImageView *img_header;

@end
