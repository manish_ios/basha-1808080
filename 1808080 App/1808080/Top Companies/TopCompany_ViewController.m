//
//  TopCompany_ViewController.m
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import "TopCompany_ViewController.h"
#import "navigator_view.h"
#import "DirectoryServices_View.h"
#import "Company_TableViewCell.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface TopCompany_ViewController ()
{
    navigator_view *navigate;
}


@end

@implementation TopCompany_ViewController
@synthesize tble_companies;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            nibNameOrNil = @"TopCompany_ViewController_6Plus";   // iPhone 4"
        }
    }
    nibBundleOrNil = nil;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    ///Hide Navigation View
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)]; // Declare the Gesture.
    gesRecognizer.delegate = self;
    [blurView addGestureRecognizer:gesRecognizer];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Handle Tap Guesture
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    blurView.hidden=YES;
    speed_view.hidden=YES;
    btn_showRight1.hidden=YES;
    btn_back.hidden=NO;
    btn_back.userInteractionEnabled=YES;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.45];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
    //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
    
    btn_showRight.tag=1;
}


-(void)viewDidAppear:(BOOL)animated
{
    navigate=[[navigator_view alloc]init];
    
    [self Get_companies];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark JSON parsing: get Directory List
-(void)Get_companies
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",myAppDelegate.BaseURL, @"services_top_companies"]];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([[[[responseObject valueForKey:@"Response"]objectAtIndex:0]valueForKey:@"success"] isEqualToString:@"S"])
         {
             arr_companies=[[[responseObject valueForKey:@"Response"] valueForKey:@"data"] objectAtIndex:0];
             NSLog(@"Array Companies:%@",arr_companies);
             
             [tble_companies reloadData];
             tble_companies.hidden=NO;
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             NSLog(@"not Found");
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"error %@",error);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
}



#pragma mark Set TableView Cell Animation
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D translation;
    // rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
    if(deviceResult.height == 480)
    {
        translation = CATransform3DMakeTranslation(0, 190, 0);
    }
    else
    {
        translation = CATransform3DMakeTranslation(0, 280, 0);
        //rotation.m34 = 1.0/ -600;
    }
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    [UIView commitAnimations];
}


#pragma mark TableView Delegate: Height For Row
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}

#pragma mark TableView Delegate: Number Of Rows
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_companies.count;
}

#pragma mark TableView Delegate: RowAtIndex
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier=@"Company_TableViewCell";
    
    tble_companies.separatorColor=[UIColor clearColor];
    
    Company_TableViewCell *cell = (Company_TableViewCell *)[tble_companies dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil)
    {
        CGSize deviceResult = [[UIScreen mainScreen] bounds].size;
        
        if(deviceResult.height == 736)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Company_TableViewCell_6Plus" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Company_TableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    cell.lbl_CompanyName.text=[[arr_companies objectAtIndex:indexPath.row]valueForKey:@"title"];
    
    cell.lbl_CompanyDesc.text=[[arr_companies objectAtIndex:indexPath.row]valueForKey:@"description"];
    
    CGRect rect = cell.imageView.frame;
    
    [cell.img_thumb setFrame:rect];
    NSURL *url1 = [NSURL URLWithString:[[arr_companies objectAtIndex:indexPath.row]valueForKey:@"pic"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url1];
    UIImage *placeholderImage = [UIImage imageNamed:@"listimg.png"];
    
    
    __weak Company_TableViewCell *weakCell = cell;
    [cell.img_thumb setImageWithURLRequest:request placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *img) {
                                       
                                       weakCell.img_thumb.image = img;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    
    weakCell.img_thumb.clipsToBounds=YES;
    
    int rate = [[[arr_companies objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue];
    
    NSArray *ratingView = [[NSArray alloc] initWithObjects:cell.imgRate1, cell.imgRate2, cell.imgRate3, cell.imgRate4,cell.imgRate5,nil];
    
    for (int k = 0; k < 5; k++) {
        UIImageView *imgView = [ratingView objectAtIndex:(k)];
        if (k < rate) {
            [imgView setImage:[UIImage imageNamed:@"staractive.png"]];
        } else {
            [imgView setImage:[UIImage imageNamed:@"star.png"]];
        }
    }

    
    ///web url
    cell.btn_web.tag=indexPath.row;
    [cell.btn_web addTarget:self action:@selector(OpenWebURL:) forControlEvents:UIControlEventTouchUpInside];
    
    ///make call
    cell.btn_Call.tag=indexPath.row;
    [cell.btn_Call addTarget:self action:@selector(Makecall:) forControlEvents:UIControlEventTouchUpInside];
    
    //send mail
    cell.btn_mail.tag=indexPath.row;
    [cell.btn_mail addTarget:self action:@selector(sendmail:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark Open Web URL
-(IBAction)OpenWebURL:(id)sender
{
    NSURL *url_flicker = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@",[[arr_companies objectAtIndex:[sender tag]]valueForKey:@"website"]]];
    
    if (![[UIApplication sharedApplication] openURL:url_flicker]) {
        NSLog(@"%@%@",@"Failed to open url:",[url_flicker description]);
    }
}

#pragma mark Make a call
-(IBAction)Makecall:(id)sender
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[[arr_companies objectAtIndex:[sender tag]]valueForKey:@"phone"]]];
    
    [[UIApplication  sharedApplication] openURL:url];
}


#pragma mark send mail
-(IBAction)sendmail:(id)sender
{
    NSLog(@"send mail");
    // Email Content
    NSString *messageBody =@"Check out this App at";
    
    // To address
    //NSArray *toRecipents = [arr_details valueForKey:@"email"];
    NSArray *toRecipents = [NSArray arrayWithObject:[[arr_companies objectAtIndex:[sender tag]] valueForKey:@"email"]];
    
    NSLog(@"toRecipents %@ ",toRecipents);
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:@"Share App Information"];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}


#pragma mark email composer delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}




#pragma mark show right slide view
-(IBAction)showRightMenu:(id)sender
{
    [self.view addSubview:speed_view];
    
    btn_back.tag=1;
    
    if ([sender tag]==1)
    {
        [self show_hide];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=2;
    }
    
    else
    {
        blurView.hidden=YES;
        speed_view.hidden=YES;
        btn_showRight1.hidden=YES;
        btn_back.hidden=NO;
        btn_back.userInteractionEnabled=YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.45];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[speed_view layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[[self.view layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        btn_showRight.tag=1;
    }
}

-(void)show_hide
{
    blurView.hidden=NO;
    btn_showRight1.hidden=NO;
    btn_back.hidden=YES;
    btn_back.userInteractionEnabled=NO;
    
    speed_view.frame=CGRectMake(60, speed_view.frame.origin.y, speed_view.frame.size.width,speed_view.frame.size.height);
    speed_view.hidden=NO;
}

#pragma mark right menu selection
-(IBAction)Rightmenuselection:(id)sender
{
    speed_view.hidden=YES;
    [self.navigationController pushViewController:[navigate DirectoryRightNavigation:[sender tag]] animated:YES] ;
}

-(IBAction)back:(id)sender
{
    DirectoryServices_View *mainview=[[DirectoryServices_View alloc]init];
    [self. navigationController pushViewController:mainview animated:YES];
}


@end
