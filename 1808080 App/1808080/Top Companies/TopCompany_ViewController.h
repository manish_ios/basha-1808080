//
//  TopCompany_ViewController.h
//  1808080
//
//  Created by RailsBox on 16/06/15.
//  Copyright (c) 2015 railsbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MessageUI.h>


@interface TopCompany_ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView *speed_view;
    IBOutlet UIView *blurView;
    
    
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_showRight;
    
    IBOutlet UIButton *btn_showRight1;

    NSMutableArray *arr_companies;
}

@property (strong, nonatomic) IBOutlet UITableView *tble_companies;


@end
